<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;



class IndexController extends AbstractActionController {
    
    /*
     * Действие по умолчанию возвращает список статей
     */
    public function indexAction() {
        $articleTable = $this->getServiceLocator()->get('ArticleTable');
        // grab the paginator from the AlbumTable
        $paginator = $articleTable->fetchAll(true);
                     
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6);
                
        return new ViewModel(array(
            'paginator' => $paginator,
        ));
        
    }   //indexAction
    
    
     
    
    
    
    
    
    
    
    
    
    
    
}   //IndexController
