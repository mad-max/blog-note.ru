<?php

namespace Application\Form;

use Zend\Form\Form;


class LoginForm extends Form {
    
    public function __construct($name=null) {
        parent::__construct('Login');
        //$this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
       
        
        $this->add(array(
            'name'=>'email',
            'attributes'=>array(
                'id' => 'email',
                'type'=>'email',
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => "Email address",
                'autofocus' => "true"
            ),
            'options'=>array(
                'label' => 'Email address',
            ),
        ));
        
        $this->add(array(
            'name'=>'password',
            'attributes'=>array(
                'id' => 'password',
                'type'=>'password',
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => "Password",
            ),
            'options'=>array(
                'label' => 'Password',
                
                
            ),
        ));
        
        $this->add(array(
            'name'=>'checkbox',
            'type'=>'Zend\Form\Element\Checkbox',
            'attributes'=>array(
                'type'=>'checkbox',
            ),
            'options'=>array(
                'label' => 'Remember me',
                'checked_value' => 1,
                'unchecked_value' => 0,
            ),
        ));
        
                
        $this->add(array(
            'name'=>'signIn',
            'attributes'=>array(
                'type'=>'submit',
                'value' => 'Sign in',
                'class' => 'btn btn-lg btn-primary btn-block',
            ),
        ));
                        
        
        
    }   //__construct


}













