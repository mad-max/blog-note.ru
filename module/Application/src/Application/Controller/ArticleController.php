<?php

namespace Application\Controller;


use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Model\Article;
use Application\Model\Category;
use Application\Model\User;
//use Application\Model\GetDateTime;

//use Application\Model\User;
//use Application\Rbac\AssertUserIdMatches;

use \Exception;




class ArticleController extends AbstractActionController {
    
    /*
     * Сервис возвращает объект DateTime для работы с датой и временем
     */
    private $dateTimeService = NULL;
    
    /*
     * Сервис возвращает объект User c данными залогиненного пользователя
     */
    private $currentUserService = NULL;
    
    /*
     * Сервис возвращает объект ArticleTable для работы с БД
     */
    private $articleTableService = NULL;
    
    
    
    /*
     * Действие по умолчанию возвращает список статей
     */
    public function indexAction() {
        
        // grab the paginator from the ArticleTable
        $paginator = $this->getArticles(); //
        
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6);
        
        // Проверка прав
        $addPermission = false;
        $rbac = $this->getServiceLocator()->get('Rbac');
        if ($rbac->checkAccess('add.article', 'not_assert')) {
            $addPermission = true;             
        }
        
        $model = new ViewModel(array(
            'paginator' => $paginator,
            'addPermission' => $addPermission,
        ));
                
        return $model;
        
    }   //indexAction
    
    /*
     * возвращает список статей отсортированных по категории
     */
//    public function categorisedAction() {
//        $categoryId = $this->params()->fromRoute('id');
//        
//        // grab the paginator from the ArticleTable
//        $paginator = $this->getArticlesByCategory($categoryId);
//          
//        // set the current page to what has been passed in query string, or to 1 if none set
//        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
//        // set the number of items per page to 10
//        $paginator->setItemCountPerPage(6);
//        
//        // Проверка прав
//        $addPermission = false;
//        $rbac = $this->getServiceLocator()->get('Rbac');
//        if ($rbac->checkAccess('add.article', 'not_assert')) {
//            $addPermission = true;             
//        }
//                
//        $model = new ViewModel(array(
//            'paginator' => $paginator,
//            'categoryId' => $categoryId,    //для работы пагинатора, иначе на следующих страницах ничего не выдает
//            'addPermission' => $addPermission,
//            ));
//        
//        return $model;
//        
//    }   //categorisedAction
    
    
    /*
     * Действие по умолчанию возвращает список категорий
     */
//    public function categoriesAction() {
//        //TODO view list of categories
//        echo "categories";
//        die();
//        
//    }   //categoriesAction
    
    
    /*
     * Просмотр одной статьи
     */
    public function viewAction() {

        $articleId = $this->params()->fromRoute('id');
        
        $articleData = $this->getArticleDataArray($articleId);
        
        return new ViewModel(array(
            'articleData' => $articleData,
        ));
        
                
    }   //viewAction
    
    /*
     * Обрабатывает запрос на редактирование статьи
     * Возвращает данные для шаблона article\edit.phtml
     */
    public function editAction() {
        
        $articleId = $this->params()->fromRoute('id');  //TODO проверка
        
        $article = $this->getArticle($articleId);
                
        $rbac = $this->getServiceLocator()->get('Rbac');
        
        //Проверка разрешения
        if (!$rbac->checkAccess('edit.article', $article)) {
            return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
        }
        
        //Создает массив с данными из объектов $article, $category, $user 
        //и форматирует их.
        $articleData = $this->getArticleDataArray($articleId);
                
        //Установить данные статьи в фому
        $form = $this->getServiceLocator()->get('ArticleEditForm');
        $categoryList = $this->getArrayCategories();
        $form->bind($article);
        $form->get('categoryId')->setValueOptions($categoryList); //Устанавливаем отдельно значения в список (select)
        $form->get('tags')->setValue($articleData['tags']);
        
        return new ViewModel(array(
            'articleData' => $articleData,
            'form' => $form,
        ));
        
    }   //EditAction
    
    
    /*
     * Выводит форму для добавления статьи
     */
    public function addAction() {
        //Проверка разрешения
        $rbac = $this->getServiceLocator()->get('Rbac');      
        if (!$rbac->checkAccess('add.article', 'not_assert')) {
            return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
        }
        
        $form = $this->getServiceLocator()->get('ArticleEditForm');
        
        $categoryList = $this->getArrayCategories();
        
        $form->get('categoryId')->setValueOptions($categoryList); //Устанавливаем отдельно значения в список (select), имя списка (categories) не совпадает со свойством categoryId класса Article
        
        return new ViewModel(array(
            'form' => $form,
        ));
    }   //AddAction
    
    
   
    
    /*
     * Сохранение статьи в бд
     */
    public function processAction() {
        
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute('application/article');
        }
        $post = $this->request->getPost();   
        
        $form = $this->getServiceLocator()->get('ArticleEditForm'); 
        
        $form->setData($post);
        if (!$form->isValid()) {    //TODO ошибка при возврате формы (нет id статьи)
            $form->get('categoryId')->setValueOptions($this->getArrayCategories()); //устанавливаем категории в список select
            if (!($form->get('id')->getValue())) {     //Редактирование статьи
                return $this->getErrorModel($form, 'application/article/add');
                
            }
            return $this->getErrorModel($form, 'application/article/edit');
            
        }
        
        $validatedData = $form->getData();
        
        $rbac = $this->getServiceLocator()->get('Rbac');
         
        if (!empty($validatedData['id'])) {    //Редактирование статьи
            //Получить статью
            $article = $this->getArticle($validatedData['id']);
            
            //Проверка разрешения
            if (!$rbac->checkAccess('edit.article', $article)) {
                return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
            }
        
            //дата обновления статьи
            $article->updated = $this->getDateTimeService()->format('Y-m-d H:i:s'); //установить текущие дату и время как дату обновления
            
            //увеличить версию при обновлении
            $article->version = $article->version + 1; 
            
        } else {    //Добавление новой статьи
            //Проверка разрешения
            if (!$rbac->checkAccess('add.article', 'not_assert')) {
                return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
            }
        
            $article = new Article();
            
            //дата создания статьи
            $article->created = $this->getDateTimeService()->format('Y-m-d H:i:s'); //установить текущие дату и время как дату создания
            
            //Устанавливаем текущего юзера в качестве автора статьи
            $article->setAuthorId($this->getCurrentUserService()->id);
        }
        
        //если в объекте не было установлено isPublished а в $post установлено то значит юзер желает опубликовать статью
        if ($article->isPublished===false and $validatedData['isPublished']==='1') {
            $article->published = $this->getDateTimeService()->format('Y-m-d H:i:s');
        }
            
               
        $article->exchangeArray($validatedData);
        
        //Проверка ссылок на изображения, вставленных пользователем. Определение их размера.
        //Создание миниатюры.
        $article->content = $this->ReplaceImgsOnThumbnails($article->content);
        
       
        //сохранить статью в бд
        $lastInsertId = $this->saveArticle($article);
        
        //Теги
        //Сначала удалить старые связи с тегами, если они есть, например, при редактировании статьи
        if (!empty($validatedData['id'])) {
            $this->deleteTagArticleLinks($validatedData['id']);
        }
        //TODO вынести в отдельный метод
        //Разбить строку на отдельные теги используя разделители
        $separ = ' ,.;:';
        $tagsArray = array();
        //разрешается только буквы и цифры
        $filter = new \Zend\I18n\Filter\Alnum();
        //разбиваем строку на токены и формируем массив
        $tag = $filter->filter(strtok($validatedData['tags'], $separ));    //получаем первый тег
        while (!empty($tag)) {
            $tagsArray[] = $tag;    //добавляем тег в массив
            $tag = $filter->filter(strtok($separ)); // исходная строка передается только при первом вызове
        }
        //убрать повторяющиеся теги
        $tagsUniqArray=array_unique($tagsArray);
        foreach ($tagsUniqArray as $tag) {
            //проверить есть ли тег в базе
            $tagId = $this->getTagIdByName($tag);
            if (empty($tagId)) {
                //если нет добавить
                $tagId = $this->saveTag($tag);
            }
            //добавить articleId-tagId в базу
            $this->saveTagArticleLink($tagId, $lastInsertId);
        }
        
        
        return $this->redirect()->toRoute('application/article', array('action' => 'view', 'id' => $lastInsertId)); 
        
    }   //processAction
    
    
    /*
     * Удалить статью из бд
     */
    public function deleteAction() {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $this->request->getPost();
            $form = $this->getServiceLocator()->get('DeleteForm'); 
        
            $form->setData($post);  
            if (!$form->isValid()) {    
                return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой
            }
            
            $validatedData = $form->getData();
            
            if ($validatedData['action'] === 'delete') {
                
                //Получить статью из БД
                $article = $this->getArticle($validatedData['id']);

                //Проверка разрешения
                $rbac = $this->getServiceLocator()->get('Rbac');
                if (!$rbac->checkAccess('delete.article', $article)) {
                    return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
                }
                
                //Удаляем статью
                $this->deleteArticle($article->id);
            }
            
            //Переход на страницу со списком статей
            return $this->redirect()->toRoute('application/article');
            
        }
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('application/article');
        }
        
        //Получить статью из БД
        $article = $this->getArticle($id);
        
        //Возвратить форму подтверждения удаления
        $form = $this->getServiceLocator()->get('DeleteForm');
        $form->get('action')->setValue('delete'); 
        $form->get('id')->setValue($article->id); 
         
        return new ViewModel(array(
            'form' => $form,
            'article' => $article,
        ));
        
        
    }   //deleteAction

    
    /*
     * Получить список категорий
     * 
     * возвращает массив id категории => название категории
     */
    private function getArrayCategories() {

       //Получить список объектов категория
        try {
            $categoryTable = $this->getServiceLocator()->get('CategoryTable');
            $categories = $categoryTable->fetchAll();
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
        
        //Установить значения в список с категориями
        $categoryList = array();
        foreach($categories as $categoryObject) {
            $categoryList[$categoryObject->id] = $categoryObject->nameCategory;
        }
        
        return $categoryList;
    }   //getArrayCategories
    
    /*
     * Получить id и name тега из базы
     * 
     * return string
     */
    private function getTagIdByName($tag) {
        try {
            $articleTable = $this->getServiceLocator()->get('ArticleTable');
            $tagId = $articleTable->getTagByName($tag)['id'];
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
        
        return $tagId;
        
    }   //getTagIdByName
    
    
    /*
     * Сохранить связь статья<->тег в базе
     * 
     * return string
     */
    private function saveTagArticleLink($tagId, $articleId) {
        try {
            return $this->getArticleTableService()->saveTagArticleLink($tagId, $articleId);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }   //saveTagArticleLink
    
    
    /*
     * Удалить связи статья<->тег в базе
     * 
     * return 
     */
    private function deleteTagArticleLinks($articleId) {
        try {
            return $this->getArticleTableService()->deleteTagArticleLinks($articleId);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }   //deleteTagArticleLinks
    
    
    /*
     * Сохранить новый тег в базе
     * 
     * return string $lastInsertId
     */
    private function saveTag($tag) {
        try {
            return $this->getArticleTableService()->saveTag($tag);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }   //saveTag
    
    
    /*
     * Создает массив с данными для вывода в представлении. Форматирует дату в
     * соответствии с часовым поясом. Если статья не опубликована, то соответствующий элемент 
     * в массиве не создается.
     */
    private function getArticleDataArray($articleId) {
        
        
        $article = $this->getArticle($articleId); 
        
        
        $currentUser = $this->getCurrentUserService();
        
        //Получить автора статьи по id
        try {
            $userTable = $this->getServiceLocator()->get('UserTable');  //TODO Если используется несколько раз вынести в отдельный приватный метод. Создать соответствующее свойство? Или как в сервис записать?
            $author = $userTable->getUser($article->authorId);    
        } catch (Exception $ex) {
            //return $this->notFoundAction();
            $this->getServiceLocator()->get('LogService')->err($ex);
            $author = new User();
//            $this->getResponse()->setStatusCode(404);
//            return;
        }
        
        //Получить категорию статьи по id
        try {
            $categoryTable = $this->getServiceLocator()->get('CategoryTable');  //TODO Если используется несколько раз вынести в отдельный приватный метод. Создать соответствующее свойство? Или как в сервис записать?
            $category = $categoryTable->getCategory($article->categoryId);  
        } catch (Exception $ex) {
            //return $this->notFoundAction();
            $this->getServiceLocator()->get('LogService')->err($ex);
            $category = new Category();
        }
               
        $articleData = array();
        $articleData['id'] = $article->id;
        $articleData['title'] = $article->title;
        $articleData['content'] = $article->content;
        $articleData['summary'] = $article->summary;
        $articleData['category'] = $category->nameCategory;
        $articleData['author'] = $author->username;
        
        //дата создания
        $articleData['created'] = $this->getDateTimeService()->setTimestamp($article->created)->
                setTimezone($currentUser->timezone)->getRusDateTime();   
        
        //дата обновления. если версия больше 1, то значит статья обновлялась и надо вывести дату обновления
        if($article->version > 1) {
            $articleData['updated'] = $this->getDateTimeService()->setTimestamp($article->updated)->
                setTimezone($currentUser->timezone)->getRusDateTime();   
        }
        //дата публикации
        if($article->isPublished) {
            $articleData['published'] = $this->getDateTimeService()->setTimestamp($article->published)->
                setTimezone($currentUser->timezone)->getRusDateTime();   
        }
        $articleData['version'] = $article->version;
        $articleData['isPublished'] = $article->isPublished ? "true" : "false";

        $rbac = $this->getServiceLocator()->get('Rbac');
        
        $articleData['editPermission'] = $rbac->checkAccess('edit.article', $article);
        $articleData['deletePermission'] = $rbac->checkAccess('delete.article', $article);
        
        //Получить теги для статьи
        $results = $this->getArticleTableService()->getTagsByArticle($articleId);
        $tagArray = array();
        foreach ($results as $result) {
            $tagArray[] = $result['tag_name'];
        }
        $tags = implode(" ", $tagArray);
        $articleData['tags'] = $tags;
                
        return $articleData;
        
    }   //getArticleDataArray
        
    
    /**
     * Создает модель с описанием ошибки (если требется) в случае если не 
     * прошла валидацию или исключение при сохранении в бд, или пароли не 
     * совпадают и т.д.
     */
    private function getErrorModel($form, $template, $errorDescription = null) {
        
        $dataModel = array(
            'error'=>true,
            'form'=>$form,
        );
        
        if (isset($errorDescription)) {
            $dataModel['description'] = $errorDescription;
        }
        
        $model=new ViewModel($dataModel);
        $model->setTemplate($template);
        
        return $model;
        
    }
    
    
    /*
     * Возвращает объект для работы с датой и временем
     */
    private function getDateTimeService() {
        if($this->dateTimeService===NULL) {
            $this->dateTimeService = $this->getServiceLocator()->get('GetDateTime');
        }
        return $this->dateTimeService;
    }
    
    /*
     * Возвращает объект User с данными залогиненного пользователя
     */
    private function getCurrentUserService() {
        if($this->currentUserService===NULL) {
            $this->currentUserService = $this->getServiceLocator()->get('CurrentUserFactory');
        }
        return $this->currentUserService;
    }
    
    /*
     * Возвращает объект ArticleTable для работы с БД
     */
    private function getArticleTableService() {
        if($this->articleTableService===NULL) {
            $this->articleTableService = $this->getServiceLocator()->get('ArticleTable');
        }
        return $this->articleTableService;
    }
    
    /*
     * Возвращает объект Article из БД по id
     */
    private function getArticle($id) {
        //Получить статью из БД
        try {
            return $this->getArticleTableService()->getArticle($id);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
    /*
     * Удаляет Article из БД по id
     */
    private function deleteArticle($id) {
        //Удалить статью из БД
        try {
            $this->getArticleTableService()->deleteArticle($id)->deleteTagArticleLinks($id);
            //$this->getArticleTableService()->deleteTagArticleLinks($id);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
        return $this;
    }
    
    /*
     * Сохраняет Article в БД. Возвращает id сохраненной статьи
     */
    private function saveArticle(Article $article) {
        //Сохранить статью в БД
        try {
            return $this->getArticleTableService()->saveArticle($article);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
    /*
     * Получить статьи по категории
     */
    private function getArticlesByCategory($id) {
        //Получить статьи
        try {
            return $this->getArticleTableService()->getArticleByCategory($id, true);    //true для работы пагинатора
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
    /*
     * Получить все статьи
     */
    private function getArticles() {
        //Получить статьи
        try {
            return $this->getArticleTableService()->fetchAll(true);    //true для работы пагинатора
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
    

    
    /**
     * Заменяет теги img на ссылки в виде миниатюр. 
     * 
     * @param string $string текст, полученный от пользователя.
     * 
     * @return string Текст с замененными тегами img на a.
     */
    private function ReplaceImgsOnThumbnails($string) {
        //Создаем документ DOM
        $document = new \DOMDocument();
        $document->loadHTML(mb_convert_encoding($string, 'HTML-ENTITIES', "UTF-8"));
        
        //Ищем теги img
        $results= $document->getElementsByTagName('img');
        if(!$results->length) {
            return $string;
        }
        
        //Проходим по всем найденным тегам и форматируем их
        foreach ($results as $result) {
            //Получаем ссылку из тега
            $url = $result->getAttribute('src');
            
            //Проверяем родительский элемент. Если а, то, возможно, миниатюра уже создана.
            if ($result->parentNode->nodeName!='a') {
                //Создаем ссылку (тег а) на оригинал изображения
                $link= $document->createElement('a');
                $link->setAttribute('href', $url);

                //Заменяем тег img на тег <a>
                $result->parentNode->replaceChild($link, $result);
                
                //Создаем новый тег <img>. 
                $src= $document->createElement('img');
                $src->setAttribute('src', $url);
                $src->setAttribute('class', 'img-responsive');
                
                //Добавляем к тегу <а> дочерний элемент тег <img>
                $link->appendChild($src);
                
            } else {
                $result->setAttribute('class', 'img-responsive');
            }
            
        }   //end foreach
                
        //Сохраняем DOM документ как html. Удаляем лишние теги и доктайп.
        return preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array(
            '<p>', '</p>', '<html>', '</html>', '<body>', '</body>'), array('', '', '', ''), 
                $document->saveHTML()));
        
    }   //ReplaceImgsOnThumbnails
    
    
    
    
    
    

    
    
}   //ArticleController

