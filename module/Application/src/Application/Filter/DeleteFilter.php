<?php

namespace Application\Filter;

use Zend\InputFilter\InputFilter;



class DeleteFilter extends InputFilter
{
    public function __construct() 
    {    
        
        $this->add(array(
            'name'=>'action',
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'id',
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
            ),
        ));
        
    }   //__construct
    
    
    
}   //DeleteFilter
