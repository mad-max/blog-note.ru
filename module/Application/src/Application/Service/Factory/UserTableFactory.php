<?php

namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Application\Model\User;
use Application\Model\UserTable;

/**
 * Создает объект для работы с таблицей users
 * 
 * @return UserTable
 */
class UserTableFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new User());
        $tableGateway = new TableGateway('users', $dbAdapter, null, $resultSetPrototype);
        $table = new UserTable($tableGateway);
        return $table;
    }
}
