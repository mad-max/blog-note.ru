<?php

namespace Application\Model;

class Article {
    
    protected $id = null;   //type="integer"
    protected $title = '';  //type="string",length=255
    protected $content = '';
    protected $summary = '';
    protected $categoryId = null;
    protected $authorId = null;
    protected $created = '';
    protected $updated = '';
    protected $published = '';
    protected $version = 1;
    protected $isPublished = false;
     
    
    /**
     * Метод __set() будет выполнен при записи данных в недоступные свойства. 
     *
     * @param string $name
     * @param mixed $value
     * @return void
     * @throws UnexpectedValueException
     */
     public function __set($name, $value) {
        $method = 'set' . ucfirst($name);
        if (method_exists($this, $method)) {
            $this->$method($value);
            return;
        }
        throw new \UnexpectedValueException(sprintf(
            'The property "%s" does not exist and cannot be set',
            $name
        ));
    }   //__set
    
    
    /**
     * Метод __get() будет выполнен при чтении данных из недоступных свойств. 
     *
     * @param string $name
     * @return mixed
     * @throws UnexpectedValueException
     */
    public function __get($name) {
        // Booleans
        if ('is' == substr($name, 0, 2)) {
            if (method_exists($this, $name)) {
                return $this->$name();
            }
        }
        // Check for a getter
        $method = 'get' . ucfirst($name);
        if (method_exists($this, $method)) {
            return $this->$method();
        }
        // Unknown
        throw new \UnexpectedValueException(sprintf(
            'The property "%s" does not exist and cannot be retrieved',
            $name
        ));
    }   //__get
    
    
    /**
     * Метод __isset() будет выполнен при использовании isset() или empty() 
     * на недоступных свойствах. 
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name) {
        return property_exists($this, $name);
    }   //__isset
    
    
    /**
     * Установить Id
     * 
     * @param integer $value
     * @return Entry
     */
    public function setId($value) {
        $this->id = (int)$value;
        return $this;
    }
    
    
    /**
     * Получить Id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    
    /**
     * Установить заголовок статьи
     *
     * @param string $value
     * @return Entry
     */
    public function setTitle($value) {
        $this->title = substr((string)$value, 0, 255);
        return $this;
    }
    
    
    /**
     * Получить заголовок статьи
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    
    /**
     * Установить текст статьи
     *
     * @param string $value
     * @return Entry
     */
    public function setContent($value) {
        $this->content = substr((string)$value, 0, 16777215);
        return $this;
    }
    
    
    /**
     * Получить текст статьи
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    
    /**
     * Установить краткое описание статьи
     *
     * @param string $value
     * @return Entry
     */
    public function setSummary($value) {
        $this->summary = substr((string)$value, 0, 255);
        return $this;
    }
    
    
    /**
     * Получить краткое описание статьи
     *
     * @return string
     */
    public function getSummary() {
        return $this->summary;
    }
    
    /**
     * Установить категорию статьи
     *
     * @param int $value
     * @return Entry
     */
    public function setCategoryId($value) {
        $this->categoryId = (int)$value;
        return $this;
    }
    
    
    /**
     * Получить категорию статьи
     *
     * @return string
     */
    public function getCategoryId() {
        return $this->categoryId;
    }
    
    
    /**
     * Установить автора статьи
     *
     * @param int $value
     * @return Entry
     */
    public function setAuthorId($value) {
        $this->authorId = (int)$value;
        return $this;
    }
    
    
    /**
     * Получить автора статьи
     *
     * @return string
     */
    public function getAuthorId() {
        return $this->authorId;
    }
    
    
    /**
     * Установить дату и время создания статьи
     *
     * @param string $value Date and Time
     * @return Entry
     */
    public function setCreated($value = NULL) {    
        $this->created = (string)$value;    
        return $this;
    }
    
    
    /**
     * Получить отформатированную дату и время создания статьи.
     *
     * @return string
     */
    public function getCreated() {
        return $this->created;
    }
    
    
    /**
     * Установить дату и время обновления статьи. 
     *
     * @param string $value Date and Time
     * @return Entry
     */
    public function setUpdated($value = NULL) {    
        $this->updated = (string)$value;    
        return $this;
    }
    
    
    /**
     * Получить отформатированнуюдату и время обновления статьи. 
     *
     * @return string
     */
    public function getUpdated() {
        return $this->updated;
    }
    
    
    /**
     * Установить дату и время публикации статьи
     *
     * @param string $value Date and Time
     * @return Entry
     */
    public function setPublished($value = NULL) {    
        $this->published = (string)$value;    //сохраняет метку времени Unix
        return $this;
    }
    
    
    /**
     * Получить отформатированнуюдату дату и время публикации статьи. Если $value 
     * равно нулю, то возвращается метка времени.
     *
     * @return string
     */
    public function getPublished() {
        return $this->published;
    }
    
    
    
    /**
     * Установить флаг публикации
     * 
     * @param bool $value true or  false
     * @return Entry
     */
    public function setIsPublished($value) {  
        $this->isPublished = (bool)$value;
        return $this;
    }
    
    
    /**
     * Получить флаг публикации
     *
     * @return bool
     */
    public function getIsPublished() {
        return $this->isPublished;
    }
    
    
    /**
     * Установить флаг публикации
     * 
     * @param bool $value true or  false
     * @return Entry
     */
    public function setVersion($value) {    
        $this->version = (int)$value;
        return $this;
    }
    
    
    /**
     * Получить флаг публикации
     *
     * @return bool
     */
    public function getVersion() {
        return $this->version;
    }
    
    
    function exchangeArray($data) {
        if (isset($data['id'])) {$this->setId($data['id']);}
        if (isset($data['title'])) {$this->setTitle($data['title']);}
        if (isset($data['content'])) {$this->setContent($data['content']);}
        if (isset($data['summary'])) {$this->setSummary($data['summary']);}
        if (isset($data['categoryId'])) {$this->setCategoryId($data['categoryId']);}
        if (isset($data['authorId'])) {$this->setAuthorId($data['authorId']);}
        if (isset($data['created'])) {$this->setCreated($data['created']);}
        if (isset($data['updated'])) {$this->setUpdated($data['updated']);}
        if (isset($data['published'])) {$this->setPublished($data['published']);}
        //if (isset($data['tags'])) {$this->setTags($data['tags']);}
        if (isset($data['isPublished'])) {$this->setIsPublished($data['isPublished']);}
        if (isset($data['version'])) {$this->setVersion($data['version']);}
        
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    
    
    
}   //Article

