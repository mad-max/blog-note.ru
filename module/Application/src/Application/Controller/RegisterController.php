<?php

namespace Application\Controller;


use Zend\Mvc\Controller\AbstractActionController;   
use Zend\View\Model\ViewModel;

use Application\Model\User;

use \Exception;

class RegisterController extends AbstractActionController {
    
    protected $authservice;
    
    /**
     * Выводит форму для регистрации
     */
    public function indexAction() {
        $form = $this->getServiceLocator()->get('RegisterForm');
        $viewModel=new ViewModel(array('form'=>$form));

        $timezoneList = $this->getTimezoneList();
        $form->get('timezone')->setValueOptions($timezoneList);  //устанавливаем часовые пояса в список
        
        return $viewModel;
        
    }   //indexAction
    
    
    
    /**
     * Обрабатывает данные из формы регистрации
     */
    public function processAction() {
        
        /*
         * Проверяем не залогинен ли уже пользователь
         */
        if ($this->getAuthService()->hasIdentity()) {
            return $this->redirect()->toRoute('application',
                    array('controller'=>'index',
                        'action'=>'index'
            ));
        }
        
        /*
         * Проверяем массив post, если ошибка, то переходим на начальную страницу
         */
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute(NULL,
                    array('controller'=>'register',
                        'action'=>'index'
            ));
        }
        $post=$this->request->getPost();
        
        $form = $this->getServiceLocator()->get('RegisterForm');
        
        /*
         * Данные, введенные в форму, добавляются еще раз, и выполняется их 
         * проверка методом isValid().
         */
        $form->setData($post);
        if (!$form->isValid()) {
            $timezoneList = $this->getTimezoneList();
            $form->get('timezone')->setValueOptions($timezoneList);  //устанавливаем часовые пояса в список
        
            return $this->getErrorModel($form);
        }
        
        $formData = $form->getData();
        
        if ($formData['password']!==$formData['confirmPassword']) {
            $timezoneList = $this->getTimezoneList();
            $form->get('timezone')->setValueOptions($timezoneList);  //устанавливаем часовые пояса в список
        
            return $this->getErrorModel($form, 'Passwords do not match.');
        }
        
        $timezoneList = $this->getTimezoneList(); //получаем часовые пояса
        
        /*
         * вставляем в элемент timezone массива вместо цифрового идентификатора текстовый 
         */
        $formData['timezone'] = $timezoneList[$formData['timezone']]; 
        
        /*
         * Создание пользователя
         */
        $user = $this->createUser($formData);
                
        /*
         * Сохраняем пользователя в бд
         */
        try {
            $userTable = $this->getServiceLocator()->get('UserTable'); 
            $userTable->saveUser($user);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            $timezoneList = $this->getTimezoneList();
            $form->get('timezone')->setValueOptions($timezoneList);  //устанавливаем часовые пояса в список
        
            return $this->getErrorModel($form, 'Email address is already registered.');
        }
        
        /*
         * Перенаправления на страницу входа
         */
        return $this->redirect()->toRoute('application/login',array(
                'action'=>'index'
            ));
        
        
    }   //processAction
    
    
    /**
     * Сохраняет User в бд. Генерирует соль. Хеширует пароль.
     */
    private function createUser(array $dataArray) {
         
        $data = $dataArray;
        $user=new User();
        
        /*
         * Получаем из настроек статическую соль для пароля
         */
        $config = $this->getServiceLocator()->get('config');
        $staticSalt = $config['static_salt']['hash'];
        
        /*
         * Генерируем динамическую соль для пароля и добавляем в массив
         * для передачи в объект User
         */
        $passwordSalt = $user->generateSalt();
        $data['passwordSalt'] = $passwordSalt;  
        
        /*
         * Вычисляем хеш из статической соли + пароль полученный из формы + 
         * динамическая соль. Добавляем в массив для передачи в объект User
         */
        $data['password'] = $user->getMd5Hash($staticSalt . $data['password'] . $passwordSalt); 
        
        //роль по умолчанию
        $data['role'] = 'anonymous';
        
        $user->exchangeArray($data);    
        
        return $user;
        
    }   //createUser
    
    
    /**
     * Получить список часовых поясов
     */
    private function getTimezoneList() {
        //return $tz = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        return $tz = \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, 'RU');
    }
    
    
    /**
     * Функция возвращает службу аутентификации
     */
    private function getAuthService() {
       if (!$this->authservice) {
           $authService = $this->getServiceLocator()->get('AuthService'); 
           $this->authservice = $authService;
       }
       return $this->authservice;
    }    //getAuthService
    
    
    /**
     * Создает модель с описанием ошибки (если требется) в случае если не 
     * прошла валидацию или исключение при сохранении в бд, или пароли не 
     * совпадают и т.д.
     */
    private function getErrorModel($form, $errorDescription = null) {
        
        $dataModel = array(
            'error'=>true,
            'form'=>$form,
        );
        
        if (isset($errorDescription)) {
            $dataModel['description'] = $errorDescription;
        }
        
        $model=new ViewModel($dataModel);
        $model->setTemplate('application/register/index'); 
        
        return $model;
        
    }
    
}   //RegisterController



















