<?php

namespace Application\Filter;

use Zend\InputFilter\InputFilter;



class LoginFilter extends InputFilter
{
    public function __construct() 
    {       
        
        $this->add(array(
            'name'=>'email',
            'required'=>true,
            'filters'=>array(
                array(
                    'name'=>'StringTrim'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'EmailAddress',
                    'options'=>array(
                        'domain'=>true,
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'password',
            'required'=>true,
            'filters'=>array(
                array(
                    'name'=>'StringTrim'
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'checkbox',
            'filters'=>array(
                array(
                    'name' => 'Int'
                ),
            ),
        ));
        
        
    }   //__construct
    
    
    
}   //LoginFilter
