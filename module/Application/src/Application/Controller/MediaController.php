<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
//use Zend\Http\Headers;

//use Application\Form\ImageUploadForm;
use Application\Model\ImageUpload;

use \Exception;

class MediaController extends AbstractActionController {
    
    /*
     * Сервис возвращает объект ImageUploadTable для работы с БД
     */
    private $imageUploadTableService = NULL;
    
    /*
     * Сервис возвращает объект  для работы с датой
     */
    private $dateTimeService = NULL;
    
    /*
     * Сервис возвращает объект User c данными залогиненного пользователя
     */
    private $currentUserService = NULL;
    
    /*
     * Действие по умолчанию возвращает список картинок
     */
    public function indexAction() {
        //Получить все картинки
        $paginator = $this->getImageUploads();
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6);
        
        // Проверка прав
        $addPermission = false;
        $rbac = $this->getServiceLocator()->get('Rbac');
        if ($rbac->checkAccess('upload.image', 'not_assert')) {
            $addPermission = true;             
        }
               
        return new ViewModel(array(
            'paginator' => $paginator,
            'addPermission' => $addPermission,
        ));
        
    }   //indexAction
    

     
    /*
     * Загружаем файл
     */
    public function uploadAction() {

        $form = $this->getServiceLocator()->get('ImageUploadForm');
        
        // Проверка прав
        $rbac = $this->getServiceLocator()->get('Rbac');
        if (!$rbac->checkAccess('upload.image', 'not_assert')) {
            return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
        
//            $viewModel = new ViewModel(array('message' => 'Access denied.'));
//            $viewModel->setTemplate('error/forbidden');
//            return $viewModel;
            
        }
        
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            
            if ($form->isValid()) {
                //Получение конфигурации из конфигурационных данных модуля
                $uploadPath = $this->getTempUploadLocation();
                
                //Сохранение выгруженного файла
                $adapter = new \Zend\File\Transfer\Adapter\Http();
                $adapter->setDestination($uploadPath);
                
                if ($adapter->receive()) {
                    $basename = 'img';
                    $validatedData = $form->getData();
                    $extension = split("[/\\.]", $validatedData['imageupload']['name'])[1];
                    $tempPath = $this->getTempUploadLocation();
                    $oldPathname = $tempPath . '/' . $validatedData['imageupload']['name'];
                    //check is image
                    $size=getimagesize ($oldPathname);
                    if(!$size) {
                        unlink($oldPathname);
                        return array('form' => $form, 'error' => true);
                    }
                    
                    $newPathname = $tempPath . '/' . $basename . '.' . $extension;
                    $newFilename = basename($this->renameFile($oldPathname, $newPathname, true));
                    
                    $imageUpload = new ImageUpload();
                    $exchange_data = array();
                    $exchange_data['label'] = $validatedData['label'];
                    $exchange_data['filename'] = $newFilename;
                    $exchange_data['authorId'] = $this->getCurrentUserService()->id;
                    $exchange_data['uploaded'] = $this->getDateTimeService()->format('Y-m-d H:i:s');
                    
                    $imageUpload->exchangeArray($exchange_data);
                    
                    $this->generateThumbnail($imageUpload->filename, $tempPath);
                    
                    $imgUploadLocation = $this->getImgUploadLocation();
                    $this->moveFile($tempPath . '/' . $imageUpload->filename, $imgUploadLocation);
                    
                    $lastInsertId = $this->saveImageUpload($imageUpload);
                    
                    return $this->redirect()->toRoute('application/media', 
                            array('action' => 'view', 'id' => $lastInsertId)); 
                    
                }
                
            }
            
        }
        return array('form' => $form);
        
    }
    
    /*
     * Удалить файл
     */
    public function deleteAction() {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $this->request->getPost();
            $form = $this->getServiceLocator()->get('DeleteForm'); 
        
            $form->setData($post);  
            if (!$form->isValid()) {    
                return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой
            }
            
            $validatedData = $form->getData();
            
            if ($validatedData['action'] === 'delete') {
                
                $imageUploadTable = $this->getImageUploadTableService();
                $imageUpload = $imageUploadTable->getImageUpload($validatedData['id']);
                
                // Проверка прав
                $rbac = $this->getServiceLocator()->get('Rbac');
                if (!$rbac->checkAccess('delete.image', $imageUpload)) {
                    return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
                }
                
                $imgPath = $this->getImgUploadLocation();
                $thumbPath = $this->getThumbsUploadLocation();
                unlink($imgPath . "/" . $imageUpload->filename);  
                unlink($thumbPath . "/th_" . $imageUpload->filename);

                $this->deleteImageUpload($imageUpload->id);

                return $this->redirect()->toRoute('application/media');
            }
        }
        
        $imageId = (int) $this->params()->fromRoute('id', 0);
        if (!$imageId) {
            return $this->redirect()->toRoute('application/media');
        }
        
        $imageUploadTable = $this->getImageUploadTableService();
        $imageUpload = $imageUploadTable->getImageUpload($imageId);
                
        //Возвратить форму подтверждения удаления
        $form = $this->getServiceLocator()->get('DeleteForm');
        $form->get('action')->setValue('delete'); 
        $form->get('id')->setValue($imageUpload->id); 
         
        return new ViewModel(array(
            'form' => $form,
            'imageUpload' => $imageUpload,
        ));
        
    }   //deleteAction
    
    
    /*
     * Показать миниатюру или картинку в натуральную величину. 
     */
    public function showImageAction() {
        
        $imageUploadId = $this->params()->fromRoute('id');
        
        $imageUpload = $this->getImageUpload($imageUploadId);
        
        
        if ($this->params()->fromRoute('subaction') == 'thumb') {
            $uploadPath = $this->getThumbsUploadLocation();
            $pathname = $uploadPath . "/th_" . $imageUpload->filename;
        } else {
            $uploadPath = $this->getImgUploadLocation();
            $pathname = $uploadPath . "/" . $imageUpload->filename;
        }
        
        $file = file_get_contents($pathname);   //Получить содержимое файла в виде одной строки.
        
        //прямой возврат ответа
        $response = $this->getEvent()->getResponse();
        $response->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment;filename="' . $pathname . '"',
        ));
        $response->setContent($file);
        return $response;
    }
    
    
    /*
     * Действие, которое выводит изображение 
     * в натуральную величину и эскиз
     */
    public function viewAction() {
           
        $imageUploadId = $this->params()->fromRoute('id');
        
        $imageUpload = $this->getImageUpload($imageUploadId);
        
        // Проверка прав
        $rbac = $this->getServiceLocator()->get('Rbac');
        $deletePermission = $rbac->checkAccess('delete.article', $imageUpload);
        
                
        $viewModel = new ViewModel(array(
            'imageUpload' => $imageUpload,
            'deletePermission' => $deletePermission
        ));
        
        return $viewModel;
        
    }
    
    /*
     * Получить ссылку для блога
     */
    public function getLinkAction() {
           
        $imageUploadId = $this->params()->fromRoute('id');
        
        $href = $this->url()->fromRoute('application/media',
            array('action' => 'view', 'id' => $imageUploadId), 
            array('force_canonical' => true)
        );
        
        $imgSrc = $this->url()->fromRoute('application/media', 
            array('action' => 'showImage', 'id' => $imageUploadId, 
            'subaction' => 'thumb'), array('force_canonical' => true));
        
        $link = '<a href=' . $href . '>' . '<img src="' . $imgSrc . '"' . '/></a>';
        
        $viewModel = new ViewModel(array(
            'link' => $link
        ));
        
        return $viewModel;
        
    }   //getLinkAction
    
    
    /*
     * Получение конфигурации из конфигурационных данных модуля
     */
    private function getTempUploadLocation() {
        $config  = $this->getServiceLocator()->get('config');
        if ($config instanceof \Traversable) { 
            $config = ArrayUtils::iteratorToArray($config);
        }
        if (!empty($config['module_config']['temprorary_upload_location'])) {
            return $config['module_config']['temprorary_upload_location'];
        } else {
            return FALSE;
        }
    }
    
    
    /*
     * Получение конфигурации из конфигурационных данных модуля
     */
    private function getThumbsUploadLocation() {
        $config  = $this->getServiceLocator()->get('config');
        if ($config instanceof \Traversable) { 
            $config = ArrayUtils::iteratorToArray($config);
        }
        if (!empty($config['module_config']['thumbs_upload_location'])) {
            return $config['module_config']['thumbs_upload_location'];
        } else {
            return FALSE;
        }
    }
    
    /*
     * Получение конфигурации из конфигурационных данных модуля
     */
    private function getImgUploadLocation() {
        $config  = $this->getServiceLocator()->get('config');
        if ($config instanceof \Traversable) { 
            $config = ArrayUtils::iteratorToArray($config);
        }
        if (!empty($config['module_config']['image_upload_location'])) {
            return $config['module_config']['image_upload_location'];
        } else {
            return FALSE;
        }
    }
    
    
    /*
     * Возвращает объект ImageUploadTable для работы с БД
     */
    private function getImageUploadTableService() {
        if($this->imageUploadTableService===NULL) {
            $this->imageUploadTableService = $this->getServiceLocator()->get('ImageUploadTable');
        }
        return $this->imageUploadTableService;
    }
    
    /*
     * Получить все картинки
     */
    private function getImageUploads() {
        try {
            return $this->getImageUploadTableService()->fetchAll(true);    //true для работы пагинатора
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
    /*
     * Возвращает объект ImageUpload из БД по id
     */
    private function getImageUpload($id) {
        try {
            return $this->getImageUploadTableService()->getImageUpload($id);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
    /*
     * Возвращает объект для работы с датой и временем
     */
    private function getDateTimeService() {
        if($this->dateTimeService===NULL) {
            $this->dateTimeService = $this->getServiceLocator()->get('GetDateTime');
        }
        return $this->dateTimeService;
    }
    
    /*
     * Сохраняет ImageUpload в БД. Возвращает id сохраненной картинки.
     */
    private function saveImageUpload(ImageUpload $image) {
                
        //Сохранить статью в БД
        try {
            return $this->getImageUploadTableService()->saveImageUpload($image);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
    /*
     * Получение конфигурации из конфигурационных данных модуля
     */
    private function getThumbsDimensions() {
        $config  = $this->getServiceLocator()->get('config');
        if ($config instanceof \Traversable) { 
            $config = ArrayUtils::iteratorToArray($config);
        }
        if (!empty($config['module_config']['thumbs_dimensions'])) {
            return $config['module_config']['thumbs_dimensions'];
        } else {
            return FALSE;
        }
    }
    
    /*
     * Создание эскиза для картинки
     */
    private function generateThumbnail($fileName, $tempPath) {
        $path = $this->getThumbsUploadLocation();
        $thumbsDimensions = $this->getThumbsDimensions();
        
        $sourceImageFileName = $tempPath . '/' . $fileName;
        $thumbnailFileName = 'th_' . $fileName;
        
        $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
        
        $thumb = $imageThumb->create($sourceImageFileName, $options = array('correctPermissions' => true));
        $thumb->resize($thumbsDimensions['width'], $thumbsDimensions['height']);
        $thumb->save($path . '/' . $thumbnailFileName);
        
        return $thumbnailFileName;
    }
    
    
    /*
     * Возвращает объект User с данными залогиненного пользователя
     */
    private function getCurrentUserService() {
        if($this->currentUserService===NULL) {
            $this->currentUserService = $this->getServiceLocator()->get('CurrentUserFactory');
        }
        return $this->currentUserService;
    }
    
    /*
     * Перемещает файл
     */
    private function moveFile($source, $target) {
        try {
            $filter = new \Zend\Filter\File\Rename($target);
            $filter->filter($source);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
        
        return $this;
        
    }   //moveFile
    
    /*
     * Переименовывает файл
     */
    private function renameFile($oldPathname, $newPathname, $rand=false) {
        try {
            $filter = new \Zend\Filter\File\Rename(array(
                "target"    => $newPathname,
                "randomize" => $rand,
            ));
            $pathname=$filter->filter($oldPathname);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
        
        return $pathname;
        
    }   //renameFile
       
    /*
     * Удаляет Image из БД по id
     */
    private function deleteImageUpload($id) {
        //Удалить из БД
        try {
            $this->getImageUploadTableService()->deleteImageUpload($id);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
        return $this;
    }    
    
}   //MediaController
