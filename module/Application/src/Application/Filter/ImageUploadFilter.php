<?php

namespace Application\Filter;

use Zend\InputFilter\InputFilter;

//use Zend\Validator\File\MimeType;
//use Zend\Validator\File\Size;
//use Zend\Validator\File\IsImage;
//use Zend\Validator\File\Extension;
//use Zend\Validator\File\ImageSize;

class ImageUploadFilter extends InputFilter
{
    public function __construct() 
    {       
        $this->add(array(
            'name'=>'id',
            'filters'=>array(
                array(
                    'name' => 'Int'
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'label',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>1,
                        'max'=>250,
                    ),
                ),
            ),
        ));
        
          
        $this->add(array(
            'name'=>'imageupload',
            'required'=>true,
            'validators'=>array(
                array(
                    'name'=>'\Zend\Validator\File\MimeType',
                    'options'=>array(
                        'image/gif', 
                        'image/jpg',
                        'image/jpeg',
                        'image/png',
                        'enableHeaderCheck' => true
                    ),
                ),
                array(
                    'name'=>'Zend\Validator\File\Size',
                    'options'=>array(
                        'min' => '10kB',
                        'max' => '4MB',
                    ),
                ),
                array(
                    'name'=>'Zend\Validator\File\ExcludeExtension',
                    'options'=>array(
                        'php,exe,html,phtml,js',
                        false,
                    ),
                ),
            ),
        ));
        
        
    }   //__construct
    
}   //ImageEditFilter


