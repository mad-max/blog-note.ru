<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

use Application\Model\User;

use \Exception;

/**
 * Извлекает данные пользователя из бд для отображения их в представлении
 */
class GetUser extends AbstractHelper implements ServiceLocatorAwareInterface {
    
    protected $services;
    
    protected $categoryTable;
    
    public function __construct($categoryTable) {
        $this->categoryTable = $categoryTable;
    }

    /**
     * @return object User
     */
    public function __invoke($userId=null) {
        $id = (int)$userId;
        if ($id==null) {
            $user = $this->getServiceLocator()->getServiceLocator()->get('CurrentUserFactory');
        } else {
            try {
                $userTable = $this->getServiceLocator()->getServiceLocator()->get('UserTable');
                $user = $userTable->getUser($id);
            } catch (Exception $ex) {
                $user = new User();
            }
        }
        
        return $user;
        
    }   //__invoke
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }
    
}   //GetUser