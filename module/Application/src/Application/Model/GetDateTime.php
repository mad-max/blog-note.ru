<?php

namespace Application\Model;

class GetDateTime {
    
    private $dt = NULL;
    private $tz = NULL;
    private $monthAr = array(
        1 => array('Января'),
        2 => array('Февраля'),
        3 => array('Марта'),
        4 => array('Апреля'),
        5 => array('Мая'),
        6 => array('Июня'),
        7 => array('Июля'),
        8 => array('Августа'),
        9 => array('Сентября'),
        10=> array('Октября'),
        11=> array('Ноября'),
        12=> array('Декабря')
    );
    
    
    
    public function __construct($time = 'now', $timezone = NULL) {
        if ($timezone!==NULL) {
            try {
                $this->tz = new \DateTimeZone($timezone);
            } catch(Exception $e) {
                echo $e->getMessage() . '<br />';
                exit('Could not create DateTimeZone');
            }
        }
        
        try {
            $this->dt = new \DateTime($time, $this->tz);
        } catch (Exception $e) {
            echo $e->getMessage() . '<br />';
            exit('Could not create DateTime');
        }    
        
    }   //__construct
    
    
    public function format($formatString) {
        
        return $this->dt->format($formatString);
      
    }   //getDateTime
    
    
    public function getRusMonth() {
        $monthNumber = $this->dt->format('m');
        return $this->format('j ') . $this->monthAr[(int)$monthNumber][0]
            . $this->format(' Y H:i:s');
    }   //getRusDate
    
    
    public function setTimezone($timezone) {
        if ($timezone!==NULL and is_object($this->tz)===false) {
            try {
                $this->tz = new \DateTimeZone($timezone);
            } catch(Exception $e) {
                echo $e->getMessage() . '<br />';
                exit('Could not create DateTimeZone');
            }
            $this->dt->setTimezone($this->tz);
        } elseif ($timezone!==NULL and is_object($this->tz)===true) {
            $this->dt->setTimezone($this->tz);
        }
        return $this;
    }   //setTimezone
    
    
}   //GetDateTime


        
 
