<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Application\Filter\ArticleEditFilter;
use Application\Form\ArticleEditForm;

/**
 * Создает форму редактирования статьи
 * 
 * @return ArticleEditForm
 */
class ArticleEditFormFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $filter = new ArticleEditFilter();
        $form = new ArticleEditForm();
        $form->setInputFilter($filter);
        return $form;
    }
}
