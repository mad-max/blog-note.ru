<?php

namespace Application\Form;

use Zend\Form\Form;


class ArticleEditForm extends Form {
    
    public function __construct($name=null) {
        parent::__construct('Edit Article');
        //$this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
       
        $this->add(array(
            'name'=>'id',
            'attributes'=>array(
                'type'=>'hidden',
            ),
        ));
        
        $this->add(array(
            'name'=>'title',
            'attributes'=>array(
                'type'=>'text',
                'required' => 'required',
                'class' => 'form-control',
            ),
            'options'=>array(
                'label'=>'Заголовок',
            ),
        ));
        
        $this->add(array(
            'name'=>'content',
            'attributes'=>array(
                'type'=>'textarea',
                'required' => 'required',
                'class' => 'form-control',
                'rows' => "30",
            ),
            'options'=>array(
                'label'=>'Статья',
            ),
        ));
        
        $this->add(array(
            'name'=>'summary',
            'attributes'=>array(
                'type'=>'text',
                'required' => 'required',
                'class' => 'form-control',
            ),
            'options'=>array(
                'label'=>'Краткое описание',
            ),
        ));
        
        $this->add(array(
            'name'=>'categoryId',
            'type' => 'Zend\Form\Element\Select',
            'attributes'=>array(
                'type'=>'select',
                'required' => 'required',
                'class' => 'form-control',
            ),
            'options'=>array(
                'label'=>'Категория',
                'disable_inarray_validator' => true,    //отключить валидатор InArray. 
            ),
        ));
        
        $this->add(array(
            'name'=>'tags',
            'attributes'=>array(
                'type'=>'text',
                'required' => 'required',
                'class' => 'form-control',
            ),
            'options'=>array(
                'label'=>'Теги',
            ),
        ));
        
        $this->add(array(
            'name'=>'isPublished',
            'type' => 'Checkbox',
            'attributes'=>array(
                'type' => 'checkbox',
            ),
            'options'=>array(
                'label'=>'Опубликовать',
                'checked_value' => 1,
                'unchecked_value' => 0,
            ),
        ));
        
        
        
                
                
        $this->add(array(
            'name'=>'submit',
            'attributes'=>array(
                'type'=>'submit',
                'value' => 'Отправить',
                'class' => 'btn btn-default',
            ),
        ));
                        
        
        
    }   //__construct
    
    
//    public function getInputFilterSpecification()
//    {
//        return array(
//            'content'  => array(
//                'filters'  => array(
//                    array('name' => 'htmlpurifier'),
//                ),
//            ),
//        );
//    }   //getInputFilterSpecification
    
    
}   //ArticleEditForm















