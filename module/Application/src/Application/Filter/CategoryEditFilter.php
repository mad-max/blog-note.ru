<?php

namespace Application\Filter;

use Zend\InputFilter\InputFilter;



class CategoryEditFilter extends InputFilter
{
    public function __construct() 
    {       

        $this->add(array(
            'name'=>'action',
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'id',
            'filters'=>array(
                array(
                    'name' => 'Int'
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'nameCategory',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>1,
                        'max'=>250,
                    ),
                ),
            ),
        ));
                
        
    }   //__construct
    
}   //CategoryEditFilter

