<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 

use Application\Filter\LoginFilter;
use Application\Form\LoginForm;

/**
 * Создает форму для входа
 * 
 * @return LoginForm
 */
class LoginFormFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $filter = new LoginFilter();
        $form = new LoginForm();
        $form->setInputFilter($filter);
        return $form;
    }
}
