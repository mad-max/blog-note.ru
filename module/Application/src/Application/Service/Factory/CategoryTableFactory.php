<?php

namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Application\Model\Category;
use Application\Model\CategoryTable;

/**
 * Создает объект для работы с таблицей category
 * 
 * @return CategoryTable
 */
class CategoryTableFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Category());
        $tableGateway = new TableGateway('category', $dbAdapter, null, $resultSetPrototype);
        $table = new CategoryTable($tableGateway, $dbAdapter);
        return $table;
    }
}
