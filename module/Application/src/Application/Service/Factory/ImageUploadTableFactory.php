<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Application\Model\ImageUpload;
use Application\Model\ImageUploadTable;

/**
 * Создает объект для работы с таблицей image_uploads
 * 
 * @return ImageUploadTable
 */
class ImageUploadTableFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new ImageUpload());
        $tableGateway = new TableGateway('image_uploads', $dbAdapter, null, $resultSetPrototype);
        $table = new ImageUploadTable($tableGateway);
        return $table;
    }
}

