<?php

namespace Application\Model;

class Category {
    
    /*
     * type="integer"
     */
    protected $id = null; 
    
    /*
     * type="string",length=255
     */
    protected $nameCategory = '';
    
    
    
    /**
     * Метод __set() будет выполнен при записи данных в недоступные свойства. 
     *
     * @param string $name
     * @param mixed $value
     * @return void
     * @throws UnexpectedValueException
     */
     public function __set($name, $value) {
        $method = 'set' . ucfirst($name);
        if (method_exists($this, $method)) {
            $this->$method($value);
            return;
        }
        throw new \UnexpectedValueException(sprintf(
            'The property "%s" does not exist and cannot be set',
            $name
        ));
    }   //__set
    
    
    /**
     * Метод __get() будет выполнен при чтении данных из недоступных свойств. 
     *
     * @param string $name
     * @return mixed
     * @throws UnexpectedValueException
     */
    public function __get($name) {
        // Booleans
        if ('is' == substr($name, 0, 2)) {
            if (method_exists($this, $name)) {
                return $this->$name();
            }
        }
        // Check for a getter
        $method = 'get' . ucfirst($name);
        if (method_exists($this, $method)) {
            return $this->$method();
        }
        // Unknown
        throw new \UnexpectedValueException(sprintf(
            'The property "%s" does not exist and cannot be retrieved',
            $name
        ));
    }   //__get
    
    
    /**
     * Метод __isset() будет выполнен при использовании isset() или empty() 
     * на недоступных свойствах. 
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name) {
        return property_exists($this, $name);
    }   //__isset
    
    
    /**
     * Установить Id
     * 
     * @param integer $value
     * @return Entry
     */
    public function setId($value) {
        $this->id = (int)$value;
        return $this;
    }
    
    
    /**
     * Получить Id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    
    /**
     * Установить название категории
     *
     * @param string $value
     * @return Entry
     */
    public function setNameCategory($value) {
        $this->nameCategory = substr((string)$value, 0, 255);
        return $this;
    }
    
    
    /**
     * Получить название категории
     *
     * @return string
     */
    public function getNameCategory() {
        return $this->nameCategory;
    }
    
    
    
    function exchangeArray($data) {
        if (isset($data['id'])) {$this->setId($data['id']);}
        if (isset($data['nameCategory'])) {$this->setNameCategory($data['nameCategory']);}
    }
    
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    
}   //class User
