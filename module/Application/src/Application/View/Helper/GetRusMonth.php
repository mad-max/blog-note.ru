<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

use \Exception;

/**
 * Извлекает даты для архива из бд для отображения их в представлении
 */
class GetRusMonth extends AbstractHelper {
    
        
    
    public function __construct($dateTime) {
        $this->dateTime = $dateTime;
    }

    /**
     * @return 
     */
    public function __invoke($numberOfMonth) {
        try {
            $result = $this->dateTime->getRusMonth($numberOfMonth);
            return $result;
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            //return $this->notFoundAction();
            return null;
        }
    }   //__invoke
    
}   //GetCategory


