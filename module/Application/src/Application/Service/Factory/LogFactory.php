<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;



/**
 * Создает объект для записи логов
 * 
 * @return logger
 */
class LogFactory implements FactoryInterface {
    
    protected $logger = NULL;
    
    public function createService(ServiceLocatorInterface $sm) {
        $pathToLogFile = $this->getLogFileLocation($sm);
        $writer = new \Zend\Log\Writer\Stream($pathToLogFile);
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $this->logger->info('-----------------New record----------------------');     
        return $this->logger;
    }
    
 
    /*
     * Получение конфигурации из конфигурационных данных модуля
     */
    private function getLogFileLocation($sm) {
        $config  = $sm->get('config');
        if ($config instanceof \Traversable) { 
            $config = ArrayUtils::iteratorToArray($config);
        }
        if (!empty($config['module_config']['log_file_location'])) {
            return $config['module_config']['log_file_location'];
        } else {
            throw new \Exception("Could not find location of log file in module config");
        }
    }
    
}
