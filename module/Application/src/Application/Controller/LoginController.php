<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use \Exception;

class LoginController extends AbstractActionController {
    
    protected $authservice;
    
    public function indexAction() {
        $this->layout('layout/login-layout');  
        $form = $this->getServiceLocator()->get('LoginForm'); 
        $viewModel=new ViewModel(array('form'=>$form));
        return $viewModel;
    }   //indexAction
    
    /*
     * Обрабатывает данные полученные с post из формы login
     */
    public function processAction() {
        
        /*
         * Проверяем массив post, если ошибка, то переходим на начальную страницу
         */
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute(NULL,
                    array('controller'=>'login',
                        'action'=>'index'
                        ));
        }
        $post=$this->request->getPost();
        
        
        $form = $this->getServiceLocator()->get('LoginForm'); 
        
        /*
         * Данные, введенные в форму, добавляются еще раз, и выполняется их 
         * проверка методом isValid().
         */
        $form->setData($post);
        if (!$form->isValid()) {
            return $this->getErrorModel($form); 
        }
        
        $validatedData = $form->getData();
         
        /*
         * проверьте корректность данных формы и воспользуйтесь методом 
         * AuthService, чтобы проверить учетные данные методом authenticate.
         */
        $this->getAuthService()->getAdapter()->setIdentity(
                $validatedData['email'])->setCredential(
                        $validatedData['password']);
        $result = $this->getAuthService()->authenticate();


        if ($result->isValid()) {
            try {
                $userTable = $this->getServiceLocator()->get('UserTable');
                $user = $userTable->getUserByEmail($this->request->getPost('email'));
            } catch (Exception $ex) {
                $this->getServiceLocator()->get('LogService')->err($ex);
                return $this->getErrorModel($form, 'There is no such user in database.');
            }
            
            $this->getAuthService()->getStorage()->write($user->email);
            
            return $this->redirect()->toRoute(NULL, array(
                'controller' => 'login',
                'action' => 'confirm'
            ));
        }
        
        return $this->getErrorModel($form, 'Email or password are incorrect.');
        
       
    }   //processAction
    
    /*
     * Функция возвращает службу аутентификации
     */
    private function getAuthService() {
        if (!$this->authservice) {
            $authService = $this->getServiceLocator()->get('AuthService'); 
            $this->authservice = $authService;
        }
        return $this->authservice;
    }    //getAuthService

   
    public function confirmAction() {
//        $user_email = $this->getAuthService()->getStorage()->read();
//        var_dump($user_email);
//        die();
//        $viewModel = new ViewModel(array(
//            'user_email' => $user_email
//        ));
//        return $viewModel;
        
//        return $this->redirect()->toRoute(NULL,
//                    array('controller'=>'article',
//                        'action'=>'index'
//                        ));
        
        return $this->redirect()->toRoute('application');
        
        
    }
    
    
    public function logoutAction() {
        $this->getAuthService()->clearIdentity();
    
        return $this->redirect()->toRoute('application');
        
    }
    
    
    /**
     * Создает модель с описанием ошибки (если требется) в случае если не 
     * прошла валидацию или исключение при сохранении в бд, или пароли не 
     * совпадают и т.д.
     */
    private function getErrorModel($form, $errorDescription = null) {
        
        $dataModel = array(
            'error'=>true,
            'form'=>$form,
        );
        
        if (isset($errorDescription)) {
            $dataModel['description'] = $errorDescription;
        }
        $this->layout('layout/login-layout');  
        $model=new ViewModel($dataModel);
        $model->setTemplate('application/login/index'); 
        
        return $model;
        
    }   //getErrorModel
    
    
    
}   //LoginController




















