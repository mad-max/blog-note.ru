<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 

use Zend\Permissions\Rbac\Rbac;
use Zend\Permissions\Rbac\Role;

use Application\Rbac\CheckAccess;

/**
 * Возвращает объект Rbac со всеми ролями и разрешениями,
 * прописанными в конфиге 
 * 
 * @return Rbac
 * @throws Exception
 */
class RbacFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $rbac_config = $sm->get('config');
        $config = $rbac_config['rbac_config'];
        
        $rbac = new Rbac();
        
        if ($config == null) {
            return $rbac;
        }

        foreach ($config['roles'] as $key => $role) {
            //Разбираем конфиг. Выделяем имя роли, родителя, массив с разрешениями
            $parent = null;
            $permissions = null;
            if (is_string($role)) {
                $roleName = $role;
            } elseif (is_array($role)) {
                $roleName = $key;
                if (isset($role['parent'])) {
                    $parent = $role['parent'];
                }
                if (isset($role['permissions'])) {
                    $permissions = $role['permissions'];
                }
            } else {
                throw new Exception("Invalid role configuration: role must be string or array.");
            }
            
            $role = new Role($roleName);
            
            //Добавляем разрешения для роли
            if (isset($permissions)) {
                foreach ($permissions as $permission) {
                    $role->addPermission($permission);
                }
            }
                                
            $rbac->addRole($role, $parent);
        }
 
        //return $rbac;
        
        $currentUser = $sm->get('CurrentUserFactory');
        return new CheckAccess($currentUser, $rbac);
        
    }
}


