<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

use \Exception;

/**
 * Извлекает теги из бд для отображения их в представлении
 */
class ArticleTags extends AbstractHelper {
    
    protected $articleTagsTable;
    
    
    public function __construct($articleTagsTable) {
        $this->articleTagsTable = $articleTagsTable;
    }
    
    /**
     * @return 
     */
    public function __invoke() {
        try {
            $results = $this->articleTagsTable->getMostUsedTags();
            return $results;
        } catch (Exception $ex) {
            //$this->getServiceLocator()->get('LogService')->err($ex);
            //return $this->notFoundAction();
            $tag = array ('tag_name' => '', 'articles_count' => '0');
            return $arr = array('tags' => $tag);
                    
        }
    }   //__invoke
    
}   //ArticleTags
