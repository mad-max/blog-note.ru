<?php

namespace Application\Filter;

use Zend\InputFilter\InputFilter;



class RegisterFilter extends InputFilter
{
    public function __construct() 
    {       

        
        $this->add(array(
            'name'=>'username',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>1,
                        'max'=>250,
                    ),
                ),
            ),
        ));
        
          
        $this->add(array(
            'name'=>'email',
            'required'=>true,
            'filters'=>array(
                array(
                    'name'=>'StringTrim'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'EmailAddress',
                    'options'=>array(
                        'domain'=>true,
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'timezone',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'Int'
                ),
            ),
        ));
//            
//        $this->add(array(
//            'name'=>'inputTimezone',
//            'required'=>true,
//            'filters'=>array(
//                array(
//                    'name' => 'StripTags'
//                ),
//                array(
//                    'name' => 'StringTrim'
//                ),
//                array(
//                    'name' => 'StripNewlines'
//                ),
//            ),
//            'validators'=>array(
//                array(
//                    'name'=>'StringLength',
//                    'options'=>array(
//                        'encoding'=>'UTF-8',
//                        'min'=>1,
//                        'max'=>250,
//                    ),
//                ),
//            ),
//        ));
        
        $this->add(array(
            'name'=>'password',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'StringTrim'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>2,
                        'max'=>250,
                    ),
                ),
            ),
        ));
        
        
        $this->add(array(
            'name'=>'confirmPassword',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'StringTrim'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>2,
                        'max'=>250,
                    ),
                ),
            ),
        ));
        
        
        
        
        
    }   //__construct
    
}   //RegisterFilter
