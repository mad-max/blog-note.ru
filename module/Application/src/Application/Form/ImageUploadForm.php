<?php

namespace Application\Form;

use Zend\Form\Form;


class ImageUploadForm extends Form {
    
    public function __construct($name = null, $options = array()) {
        parent::__construct('upload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
       
        $this->add(array(
            'name'=>'id',
            'attributes'=>array(
                'type'=>'hidden',
            ),
        ));
        
        $this->add(array(
            'name'=>'label',
            'attributes'=>array(
                'type'=>'text',
                'required' => 'required',
                'class' => 'form-control',
            ),
            'options'=>array(
                'label'=>'Название изображения',
            ),
        ));
        
        $this->add(array(
            'name'=>'imageupload',
            'attributes'=>array(
                'type'=>'file',
                'required' => 'required',
            ),
            'options'=>array(
                'label'=>'Изображение для загрузки',
            ),
        ));
        
        $this->add(array(
            'name'=>'submit',
            'attributes'=>array(
                'type'=>'submit',
                'value' => 'Отправить',
                'class' => 'btn btn-default',
            ),
        ));
                        
        
        
    }   //__construct
    
    
    
}   //ImageUploadForm















