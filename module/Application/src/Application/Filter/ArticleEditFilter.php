<?php

namespace Application\Filter;

use Zend\InputFilter\InputFilter;



class ArticleEditFilter extends InputFilter
{
    public function __construct() 
    {       

        
        $this->add(array(
            'name'=>'id',
            'filters'=>array(
                array(
                    'name' => 'Int'
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'title',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>1,
                        'max'=>250,
                    ),
                ),
            ),
        ));
        
          
        $this->add(array(
            'name'=>'content',
            'required'=>true,
            'filters'=>array(
//                array(
//                    'name' => 'StripTags',
//                    'options' => array(
//                        'allowTags' => array(
//                            'b' => array(),
//                            'i' => array(),
//                            'a' => array(
//                                'href'
//                            ),
//                            'img' => array(
//                                'src',
//                                'width'
//                            ),
//                        ),
//                    ),
//                ),
//                array(
//                    'name' => 'StringTrim'
//                ),
                array(
                    'name' => 'Application\Filter\HTMLPurifierFilter',
                    'options' => array(
                        array(
                            'HTML.Doctype' , 'HTML 4.01 Transitional',
                        ),
                        array(
                            'HTML.Allowed' , 'pre,br,b,i,ol,ul,li,img[src|height|width|alt|class],blockquote,code,a[href]',
                            
                            //'CSS.MaxImgLength' , '20px',
                            'HTML.MaxImgLength' , '500',
                        ),
//                        array(
//                            'AutoFormat.AutoParagraph', true,
//                        ),
//                        array(
//                            'Output.TidyFormat',
//                            true
//                        ),
//                        array(
//                            'AutoFormat.RemoveEmpty', true,
//                        ),
//                        array(
//                            'AutoFormat.RemoveEmpty.RemoveNbsp', true,
//                        ),
                        
                    ),
                ),
//                array(
//                    'name' => 'StripTags',
//                    'options' => array(
//                        'allowTags' => array(
//                            'p' => array(),
//                            'b' => array(),
//                            'i' => array(),
//                            'ol' => array(),
//                            'ul' => array(),
//                            'li' => array(),
//                            'img' => array(
//                                'src',
//                                'alt'
//                            ),
//                            'blockquote' => array(),
//                            'code' => array(),
//                            'a' => array(
//                                'href'
//                            ),
//                        ),
//                    ),
//                ),
//                array(
//                    'name' => 'StringTrim'
//                ),
                    
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>1,
                        'max'=>10000,
                    ),
                ),
            ),
        ));
        
        
            
        $this->add(array(
            'name'=>'summary',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>1,
                        'max'=>250,
                    ),
                ),
            ),
        ));
        
        
        $this->add(array(
            'name'=>'categoryId',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'Int'
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'tags',
            'required'=>true,
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
//                array(
//                    'name' => 'Alnum',
//                    'options' => array(
//                        'allowWhiteSpace' => true,
//                    ),
//                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>1,
                        'max'=>40,
                    ),
                ),
                array(
                    'name'=>'Regex',
                    'options'=>array(
                        'pattern' => '/^[A-Z0-9,.;: ]+$/i',  //разрешены только английские буквы и цифры
                        'messages' => array(
                            'regexNotMatch' => "Разрешены цифры и английские буквы.",
                        ),
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'isPublished',
            'filters'=>array(
                array(
                    'name' => 'Int'
                ),
            ),
        ));
        
        
        
        
    }   //__construct
    
}   //ArticleEditFilter
