<?php

namespace Application\Filter;

use Zend\InputFilter\InputFilter;



class SearchFilter extends InputFilter
{
    public function __construct() 
    {       
                
        $this->add(array(
            'name'=>'searchQuery',
            'filters'=>array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripNewlines'
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>1,
                        'max'=>250,
                    ),
                ),
            ),
        ));
        
        
        
        
    }   //__construct
    
    
    
}   //SearchFilter
