<?php

namespace Application\Form;

use Zend\Form\Form;


class SearchForm extends Form {
    
    public function __construct($name=null) {
        parent::__construct('Search');
        $this->setAttribute('class', 'navbar-form navbar-right');
        $this->setAttribute('accept-charset', 'utf-8');
        
        
        $this->add(array(
            'name'=>'searchQuery',
            'attributes'=>array(
                'type'=>'text',
                'class' => 'form-control',
                'placeholder' => "Поиск",
            ),
        ));
            
        $this->add(array(
            'name'=>'find',
            'attributes'=>array(
                'type'=>'submit',
                'value' => 'Поиск',
                'class' => 'btn btn-success',
            ),
        ));
                        
        
        
    }   //__construct


}