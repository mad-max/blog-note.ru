<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

//use Application\Model\User;

//use \Exception;

/**
 * Форматирует дату и время в соответствии с часовым поясом для отображения их в представлении
 */
class FormatDate extends AbstractHelper implements ServiceLocatorAwareInterface {
    
    protected $services;
    
    protected $dateTimeService;
    
    protected $currentUser;
    
    public function __construct($dateTimeService, $currentUser) {
        $this->dateTimeService = $dateTimeService;
        $this->currentUser = $currentUser;
    }

    // setTimestamp($v->uploaded)->setTimezone($currentUser->timezone)->getRusDateTime();
            
    /**
     * @return string
     */
    public function __invoke($date=null) {
        $date = (string)$date;
        $formatDate = $this->dateTimeService->setTimestamp($date)->
                setTimezone($this->currentUser->timezone)->getRusDateTime();
                
        return $formatDate;
        
    }   //__invoke
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }
    
}   //FormatDate