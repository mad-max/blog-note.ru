<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

use \Exception;

/**
 * Извлекает даты для архива из бд для отображения их в представлении
 */
class ArticleArchive extends AbstractHelper {
    
    protected $articleTable;
    
    
    public function __construct($ArticleTable) {
        $this->articleTable = $ArticleTable;
    }

    /**
     * @return 
     */
    public function __invoke() {
        try {
            $results = $this->articleTable->getArrayOfArchiveRecentDates();
            return $results;
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            //return $this->notFoundAction();
            return null;
        }
    }   //__invoke
    
}   //GetCategory
