<?php

namespace Application\Service\Invokables;

class GetDateTime {
    
    private $dt = NULL;
    private $tz = NULL;
    private $monthAr = array(
        1 => array('Января'),
        2 => array('Февраля'),
        3 => array('Марта'),
        4 => array('Апреля'),
        5 => array('Мая'),
        6 => array('Июня'),
        7 => array('Июля'),
        8 => array('Августа'),
        9 => array('Сентября'),
        10=> array('Октября'),
        11=> array('Ноября'),
        12=> array('Декабря')
    );
    
    private $monthArray = array(
        1 => array('Январь'),
        2 => array('Февраль'),
        3 => array('Март'),
        4 => array('Апрель'),
        5 => array('Май'),
        6 => array('Июнь'),
        7 => array('Июль'),
        8 => array('Август'),
        9 => array('Сентябрь'),
        10=> array('Октябрь'),
        11=> array('Ноябрь'),
        12=> array('Декабрь')
    );
    
    
    public function __construct($time = 'now', $timezone = NULL) {
        if ($timezone!==NULL) {
            try {
                $this->tz = new \DateTimeZone($timezone);
            } catch(Exception $e) {
                echo $e->getMessage() . '<br />';
                exit('Could not create DateTimeZone');
            }
        }
        
        try {
            $this->dt = new \DateTime($time, $this->tz);
        } catch (Exception $e) {
            echo $e->getMessage() . '<br />';
            exit('Could not create DateTime');
        }    
        
    }   //__construct
    
    public function setTimestamp($value) {
        if (is_string($value)) {
            $time = strtotime($value);
        } else {
            $time = $value;
        }
        
        try {
            $this->dt->setTimestamp ($time);
        } catch (Exception $e) {
            echo $e->getMessage() . '<br />';
            exit('Could not set timestamp');
        }    
        return $this;
    }   //setTimestamp
    
    
    public function format($formatString) {
        
        return $this->dt->format($formatString);
      
    }   //getDateTime
    
    
    public function getRusMonth($numberOfMonth=NULL) {
        $num = (int) $numberOfMonth;
        if ($num < 1 or $num >12) {
            return "Invalid value";
        }
        return $this->monthArray[$num][0];
    }   //getRusMonth
    
//    public function getNumberOfRusMonth($month=NULL) {
//        $nameMonth= (string) $month;
//        return array_search($nameMonth, monthArray);
//    }   //getRusMonth
    
    
    public function getRusDateTime() {
        $monthNumber = $this->dt->format('m');
        return $this->format('j ') . $this->monthAr[(int)$monthNumber][0]
            . $this->format(' Y H:i:s');
    }   //getRusDateTime
    
    
    public function setTimezone($timezone) {
        if (isset($timezone)) {
            try {
                $this->tz = new \DateTimeZone($timezone);
            } catch(Exception $e) {
                echo $e->getMessage() . '<br />';
                exit('Could not create DateTimeZone');
            }
            $this->dt->setTimezone($this->tz);
        }
        return $this;
    }   //setTimezone
    
    
}   //GetDateTime


        
 
