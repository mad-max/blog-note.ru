<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Application\Filter\CategoryEditFilter;
use Application\Form\CategoryEditForm;

/**
 * Создает форму редактирования статьи
 * 
 * @return ArticleEditForm
 */
class CategoryEditFormFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $filter = new CategoryEditFilter();
        $form = new CategoryEditForm();
        $form->setInputFilter($filter);
        return $form;
    }
}


