<?php

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Sql;




class ArticleTable {
    
    protected $tableGateway;
    protected $dbAdapter;
    
    
    public function __construct(TableGateway $tableGateway, \Zend\Db\Adapter\Adapter $dbAdapter) {
        $this->tableGateway=$tableGateway;
        $this->dbAdapter=$dbAdapter;
        
    }
    
    
    
    /**
     * Извлечь статьи определенной категории
     * 
     * @return paginator or array objects article
     */
    public function getArticleByCategory($categoryId, $paginated = false) {
        $categoryId=(int)$categoryId;

        if ($paginated) {
            // create a new Select object for the table articles
            $select = new Select('articles');
            $select->where(array('categoryId' => $categoryId));
            $select->order('id DESC'); // сортировка 'id' DESC
            // create a new result set based on the article entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Article());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
         
        $resultSet = $this->tableGateway->select();
        return $resultSet;
        
        
    }   //getArticleByCategory

    /**
     * Извлечь статьи для архива
     * 
     * @return 
     */
    public function getArticleArchive($year=NULL, $month=NULL, $paginated = false) {
        
        
        if ($paginated) {
            // create a new Select object for the table articles
            $select = new Select('articles');
            if (isset($month)) {
                $select->where(new Expression('MONTH(created) = ?', $month));
            }
            if (!isset($year)) {
                throw new \Exception("Function Archive. Year is required.");
            }
            $select->where(new Expression('YEAR(created) = ?', $year));
            $select->order('created DESC');
            
            // create a new result set based on the article entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Article());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        
        $resultSet = $this->tableGateway->select();
        return $resultSet;
        
    }   //getArticleArchive
    
    /**
     * Извлечь имя тега и кол-во статей с этим тегом из таблицы
     * 
     * @return array objects category
     */
    public function getTags($paginated = false) {
        
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select();
        $select->from('articles_tags');
        $select->join('tags', 'articles_tags.tag_id = tags.id', array('tag_name'), $select::JOIN_LEFT);
        $select->group('tags.id');
        $select->columns(array('articles_count' => new Expression('COUNT(articles_tags.tag_id)')));
            
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter()
            );
            $paginator = new Paginator($paginatorAdapter);
            
            return $paginator;
        
        }
        
//        $sql = new Sql($this->dbAdapter);
//        $select = $sql->select();
//        $select->from('articles_tags');
//        $select->join('tags', 'articles_tags.tag_id = tags.id', array('tag_name'), $select::JOIN_LEFT);
//        $select->group('tags.id');
//        $select->columns(array('articles_count' => new Expression('COUNT(articles_tags.tag_id)')));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        return $results;    
        
    }   //getTags
    
    
    /**
     * Извлечь имя тегов для определенной статьи
     * 
     * @return array objects category
     */
    public function getTagsByArticle($id=null) {

        $sql = new Sql($this->dbAdapter);
        $select = $sql->select();
        $select->from('articles_tags');
        $select->join('tags', 'articles_tags.tag_id = tags.id', array('tag_name'), $select::JOIN_LEFT);
        $select->group('tags.id');
        $select->where('articles_tags.article_id=' . $id);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        return $results;    
        
    }   //getTagsByArticle
    
    
    /**
     * Извлечь теги с самым большим количеством статей
     * 
     * @return array objects category
     */
    public function getMostUsedTags() {

        $sql = new Sql($this->dbAdapter);
        $select = $sql->select();
        $select->from('articles_tags');
        $select->join('tags', 'articles_tags.tag_id = tags.id', array('tag_name'), $select::JOIN_LEFT);
        $select->group('tags.id');
        $select->columns(array('articles_count' => new Expression('COUNT(articles_tags.tag_id)')));
        $select->order('articles_count DESC');
        $select->limit(10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        return $results;    
        
    }   //getTags
    
    
    /**
     * Извлечь имя и id тега из таблицы
     * 
     * @return array tag id => name
     */
    public function getTagByName($tag) {

        $sql = new Sql($this->dbAdapter);
        $select = $sql->select();
        $select->from('tags');
        $select->where(array('tag_name' => $tag));
        $select->group('tags.id');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row=$results->current();
        return $row;    
        
    }   //getTagByName
    
    
    /**
     * Сохранить новый тег в базе
     * 
     * @return string $lastInsertId
     */
    public function saveTag($tag) {

        $sql = new Sql($this->dbAdapter);
        $insert = $sql->insert('tags');
        $insert->values(array(
             'tag_name' => $tag,
        ));
        $statement = $sql->prepareStatementForSqlObject($insert);
        $statement->execute();
        $lastInsertId = $this->dbAdapter->getDriver()->getLastGeneratedValue();
        
        return $lastInsertId;
        
    }   //saveTag
    
    
    /**
     * Сохранить связь статья-тег в базе
     * 
     * @return string $lastInsertId
     */
    public function saveTagArticleLink($tagId, $articleId) {

        $sql = new Sql($this->dbAdapter);
        $insert = $sql->insert('articles_tags');
        $insert->values(array(
            'article_id' => $articleId,
            'tag_id' => $tagId,
        ));
        $statement = $sql->prepareStatementForSqlObject($insert);
        $statement->execute();
        $lastInsertId = $this->dbAdapter->getDriver()->getLastGeneratedValue();
        
        return $lastInsertId;
        
    }   //saveTagArticleLink
    
    
    /**
     * Удалить связи статья-тег в базе по id статьи
     * 
     * @return string $lastInsertId
     */
    public function deleteTagArticleLinks($articleId) {
        $sql = new Sql($this->dbAdapter);
        $delete = $sql->delete('articles_tags');
        $delete->where(array(
            'article_id' => $articleId,
        ));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $statement->execute();
        return $this;        
    }   //deleteTagArticleLinks
    
    
    /**
     * Извлечь статью по тегу из таблицы
     * 
     * @return array objects category
     */
    public function getArticlesByTag($tag=NULL, $paginated = false) {

        if ($paginated) {
            $sql = new Sql($this->dbAdapter);
            $select = $sql->select();
            $select->from('tags');
            $select->where(array('tag_name' => $tag));
            $select->columns(array());
            $select->join('articles_tags', 'tags.id = articles_tags.tag_id', array(), $select::JOIN_LEFT);
            $select->join('articles', 'articles_tags.article_id = articles.id', array('*'), $select::JOIN_LEFT);
            $select->order('created DESC');
            //$statement = $sql->prepareStatementForSqlObject($select);
            //$results = $statement->execute();
            
            // create a new result set based on the article entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Article());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        
        $resultSet = $this->tableGateway->select();
        return $resultSet; 
        
    }   //getArticlesByTag
    
    
    /*
     * возвращает годы и месяцы для которых доступны архивные данные
     */
    public function getArrayOfArchiveDates() {
    
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select('articles');
        
        $select->columns(array('year' => new Expression('YEAR(created)'), 
                'month' => new Expression('MONTH(created)'), 'total' => new Expression('COUNT(*)')));
        $select->order('created DESC');
        $select->group('year');
        $select->group('month');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        $archive = array();
        foreach ($results as $result) {
            $year = $result['year'];
            $month = $result['month'];
            $total = $result['total'];
            $archive[$year][] = [$month => $total];
        }
        
        return $archive;
        
        
    }   //getArrayOfArchiveDates
    
    
    /*
     * возвращает годы и месяцы для которых доступны архивные данные
     */
    public function getArrayOfArchiveRecentDates() {
    
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select('articles');
        
        $select->columns(array('year' => new Expression('YEAR(created)'), 
                'month' => new Expression('MONTH(created)'), 'total' => new Expression('COUNT(*)')));
        $select->order('created DESC');
        $select->group('year');
        $select->group('month');
        $select->limit(12);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        $archive = array();
        foreach ($results as $result) {
            $year = $result['year'];
            $month = $result['month'];
            $total = $result['total'];
            $archive[$year][] = [$month => $total];
        }
        
        return $archive;
        
        
    }   //getArrayOfArchiveDates
    
    
    
    /**
     * Извлечь все статьи из таблицы
     * 
     * @return paginator or array objects article
     */
    public function fetchAll($paginated=false) {
        
        if ($paginated) {
            // create a new Select object for the table articles
            $select = new Select('articles');
            $select->order('id DESC'); // сортировка 'id' DESC
            // create a new result set based on the article entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Article());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            
            $paginator = new Paginator($paginatorAdapter);
                        
            return $paginator;
        }
         
        $resultSet = $this->tableGateway->select();
        return $resultSet;
        
    }   //fetchAll
    
    /**
     * Извлечь статью из таблицы
     * 
     * @return array object article
     */
    public function getArticle($id) {
        $id=(int)$id;
        $rowset=$this->tableGateway->select(array('id'=>$id));  //получаем простой объект ResultSet на основе данных Adapter
        $row=$rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }   //getArticle
    
    
    /**
     * Сохранить статью в таблице
     * 
     * @return array object article
     */
    public function saveArticle(Article $article) {
        
        $data=array(
            'title'=>$article->title,
            'content'=>$article->content,
            'summary'=>$article->summary,
            'categoryId'=>$article->categoryId,
            'authorId'=>$article->authorId,
            'created'=>$article->created,
            'updated'=>$article->updated,
            'published'=>$article->published,
            'version'=>$article->version,
            'isPublished'=>$article->isPublished,
        );
        $id=(int)$article->id;
        if (empty($id)) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getArticle($id)) {
                $this->tableGateway->update($data, array('id'=>$id));
                return $id;
            } else {
                throw new \Exception('Article ID does not exist');
            }
        }
        
        
        
    }   //saveArticle
    
    public function deleteArticle($id) {
        $this->tableGateway->delete(array('id' => $id));
        return $this; 
    }
    
    
    
    
    
}   //ArticleTable

