<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 

use Application\Filter\DeleteFilter;
use Application\Form\DeleteForm;

/**
 * Создает форму для подтверждения удаления 
 * 
 * @return DeleteForm
 */
class DeleteFormFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $filter = new DeleteFilter();
        $form = new DeleteForm();
        $form->setInputFilter($filter);
        return $form;
    }
}
