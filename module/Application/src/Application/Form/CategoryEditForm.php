<?php

namespace Application\Form;

use Zend\Form\Form;


class CategoryEditForm extends Form {
    
    public function __construct($name=null) {
        parent::__construct('Edit Category');
        //$this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
       
        $this->add(array(
            'name'=>'action',
            'attributes'=>array(
                'type'=>'hidden',
                'value' => '',
            ),
        ));
        
        $this->add(array(
            'name'=>'id',
            'attributes'=>array(
                'type'=>'hidden',
            ),
        ));
        
        $this->add(array(
            'name'=>'nameCategory',
            'attributes'=>array(
                'type'=>'text',
                'required' => 'required',
                'class' => 'form-control',
            ),
            'options'=>array(
                'label'=>'Категория',
            ),
        ));
              
        $this->add(array(
            'name'=>'submit',
            'attributes'=>array(
                'type'=>'submit',
                'value' => 'Отправить',
                'class' => 'btn btn-default',
            ),
        ));
                        
        
        
    }   //__construct
    
    
    
}   //CategoryEditForm















