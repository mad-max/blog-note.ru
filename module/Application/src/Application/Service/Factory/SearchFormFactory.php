<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 

use Application\Filter\SearchFilter;
use Application\Form\SearchForm;

/**
 * Создает форму для поиска
 * 
 * @return SearchForm
 */
class SearchFormFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $filter = new SearchFilter();
        $form = new SearchForm();
        $form->setInputFilter($filter);
        return $form;
    }
}
