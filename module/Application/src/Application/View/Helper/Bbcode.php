<?php

namespace Application\View\Helper;

use Decoda\Decoda;
use Decoda\Hook\EmoticonHook;
use Zend\View\Helper\AbstractHelper;


class Bbcode extends AbstractHelper {
    
    public function __invoke($request){
        //$url = $this->view->basePath();
        $bbcode = new Decoda($request);
        $bbcode->defaults();
//        $bbcode->addFilter(new \Decoda\Filter\TextFilter());
//        $bbcode->addFilter(new \Decoda\Filter\ImageFilter());
//        var_dump(__DIR__ . '/../../../../data/emoticons/');
//        die();
        //$bbcode->addPath(__DIR__ . '/../../../../data/emoticons/');
        //$bbcode->addHook(new EmoticonHook(array('path' => './emoticons/')));
        //$bbcode->reset($request);
        return $bbcode->parse();
    }
}
