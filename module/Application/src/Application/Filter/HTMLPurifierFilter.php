<?php

namespace Application\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;
use HTMLPurifier;
use HTMLPurifier_Config;
use HTMLPurifier_ConfigSchema;



final class HTMLPurifierFilter extends AbstractFilter
{
    /** @var HTMLPurifier */
    private $htmlPurifier;
    
    /** @var HTMLPurifier_ConfigSchema */
    private $configSchema;
    
    /**
    * Returns the result of filtering $value
    *
    * @param mixed $value
    * @throws Exception\RuntimeException If filtering $value is impossible
    * @return mixed
    */
    public function filter($value)
    {
        
        
        //$string = $this->escapeHtmlInCodeTag($value);   //экранируем содержимое тега <code>
        
        
        
        // Allow simple <code> or <pre> tags for posting code
        $string = preg_replace_callback("/\<code\>(.*?)\<\/code\>/is", 
                array(get_class($this), 'custom_code_tag_callback'), $value);   //экранируем содержимое тега <code>
        $string = preg_replace_callback("/\<pre\>(.*?)\<\/pre\>/is", 
                array(get_class($this), 'custom_pre_tag_callback'), $string);
        //$string = preg_replace_callback("/\<code\>(.*?)\<\/code\>/is", 'custom_code_tag_callback', $string);
        //$string = preg_replace_callback("/\<pre\>(.*?)\<\/pre\>/is", 'custom_pre_tag_callback', $string);
        
        //расставляем тег br
        $string = $this->nl2br_special($string);

        $purifier = $this->getHTMLPurifier();   //создаем объект purifier
        return $purifier->purify($string);      //форматируем
                
    }   //filter
    
    
    function custom_code_tag_callback($code) {
        return '<code>'.trim(htmlspecialchars($code[1])).'</code>';
    }

    function custom_pre_tag_callback($code) {
        return '<pre><code>'.trim(htmlspecialchars($code[1])).'</code></pre>';
    }

    private function nl2br_special($string){

        // Step 1: Add <br /> tags for each line-break
        $string = nl2br($string); 

        // Step 2: Remove the actual line-breaks
        $string = str_replace("\n", "", $string);
        $string = str_replace("\r", "", $string);

        // Step 3: Restore the line-breaks that are inside <pre></pre> tags
        if(preg_match_all('/\<pre\>(.*?)\<\/pre\>/', $string, $match)){
            foreach($match as $a){
                foreach($a as $b){
                $string = str_replace('<pre>'.$b.'</pre>', "<pre>".str_replace("<br />", PHP_EOL, $b)."</pre>", $string);
                }
            }
        }
        
        // Step 4: Removes extra <br /> tags

        // Before <pre> tags
        $string = str_replace("<br /><br /><br /><pre>", '<br /><br /><pre>', $string);
        // After </pre> tags
        $string = str_replace("</pre><br /><br />", '</pre><br />', $string);

        // Arround <ul></ul> tags
        $string = str_replace("<br /><br /><ul>", '<br /><ul>', $string);
        $string = str_replace("</ul><br /><br />", '</ul><br />', $string);
        // Inside <ul> </ul> tags
        $string = str_replace("<ul><br />", '<ul>', $string);
        $string = str_replace("<br /></ul>", '</ul>', $string);

        // Arround <ol></ol> tags
        $string = str_replace("<br /><br /><ol>", '<br /><ol>', $string);
        $string = str_replace("</ol><br /><br />", '</ol><br />', $string);
        // Inside <ol> </ol> tags
        $string = str_replace("<ol><br />", '<ol>', $string);
        $string = str_replace("<br /></ol>", '</ol>', $string);

        // Arround <li></li> tags
        $string = str_replace("<br /><li>", '<li>', $string);
        $string = str_replace("</li><br />", '</li>', $string);

        return $string;
    }


//    private function escapeHtmlInCodeTag($string=null) {
//        
//        try {   //TODO разобраться почему не срабатывает исключение
//            $html = mb_convert_encoding($string, 'HTML-ENTITIES', "UTF-8");
//
//            $document = new \DOMDocument(); //создаем объект DOM
//            libxml_use_internal_errors(true);   //TODO разобраться
//            $document->loadHTML($html);     //загружаем текст и строим из него дерево дом
//
//            $document->preserveWhiteSpace = false;
//            $document->validateOnParse = false;
//            $document->formatOutput = false;
//
//            $nodes = $document->getElementsByTagName("code");   //получаем список узлов <code>
//
//            $count = count($nodes);                             //считаем кол-во таких узлов
//            if($count>=1) {
//                foreach ($nodes as $node) {                     //разбиваем список
//
//                    $tempDoc = new \DOMDocument();              //для каждого узла создаем новый ДОМ
//                    $tempDoc->appendChild($tempDoc->importNode($node,true));    //импортируем узел <code>
//
//                    $innerCode = $tempDoc->saveHTML();                          //сохраняем в строку
//
//                    $innerCode = $this->trimCodeTags($innerCode);   //обрезаем теги с начала и с конца
//
//                    while ($node->hasChildNodes()){                 //пока узел <code> имеет дочерние узлы
//                        $node->removeChild($node->firstChild);      //удаляем их
//                    }
//
//                    $node->nodeValue = htmlspecialchars($innerCode, ENT_QUOTES);    //экранируем html и сохраняем в узел <code> содержимое уже без дочерних узлов
//
//                }   //end foreach
//
//               return $document->saveHTML();    //сохраняем весь документ в строку
//
//            }   //end if
//        }  catch (Exception $ex) {
//            die($ex->getMessage());
//        }
//        
//        return $string;
//        
//    }   //escapeHtmlInCodeTag

    
//    private function trimCodeTags($string) {
//        
//        $delOpenTagCode = mb_stristr($string, '<code>', false, 'UTF-8');    //строка до первого <code>
//        if (!mb_strlen($delOpenTagCode, 'UTF-8')) {                         //если длина = 0
//            $delOpenTagCode = $string;                                      //то присваиваем исходную строку
//        } else {                                                            //и пропускаем след код
//            $delOpenTagCode = mb_substr($delOpenTagCode, 6, null, 'UTF-8'); //если не 0, обрезаем <code>
//            if (!mb_strlen($delOpenTagCode, 'UTF-8')) {                     //если длина ноль
//                $delOpenTagCode = $string;                                  //то опять присваиваем исходную строку
//            }
//        }
//
//       $delCloseTagCode = mb_strrichr($delOpenTagCode, '</code>', true, 'UTF-8');   //обрезаем с конца </code>
//        if (!mb_strlen($delCloseTagCode, 'UTF-8')) {                                //если длина ноль
//            $delCloseTagCode = $delOpenTagCode;                                     //присваиваем строку с удаленным открывающим тегом <code>
//        }
//        
//        return $delCloseTagCode;
//        
//    }   //trimCodeTags

    /**
    * @return HTMLPurifier
    */
    private function getHTMLPurifier()
    {
        if (! $this->htmlPurifier) {
            if ($this->configSchema) {
                $config = HTMLPurifier_Config::createDefault();
                foreach ($this->configSchema as $option) {
                    $config->set($option[0], $option[1]);
                }
            } else {
                $config = null;
            }
            $this->htmlPurifier = new HTMLPurifier($config);
        }
        return $this->htmlPurifier;
    }
    
    
    /**
    * @param HTMLPurifier_ConfigSchema $schema
    */
    public function setConfigSchema($schema)
    {
        $this->configSchema = $schema;
    }
    
    
    /**
    * @param HTMLPurifier $purifier
    */
//    public function setHtmlPurifier($purifier)
//    {
//        $this->htmlPurifier = $purifier;
//    }

    public function __construct($options) 
    {
        $this->setConfigSchema($options);
    }

}