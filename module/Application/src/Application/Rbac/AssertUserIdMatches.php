<?php

namespace Application\Rbac;

use Zend\Permissions\Rbac\AssertionInterface;
use Zend\Permissions\Rbac\Rbac;

class AssertUserIdMatches implements AssertionInterface {
    protected $user;
    protected $article;

    public function __construct($user) {
        $this->user = $user;
    }

    public function setArticle($article) {
        $this->article = $article;
    }

    public function assert(Rbac $rbac) {
        if (!$this->article) {
            return false;
        }
        return $this->user->id == $this->article->authorId;
    }
    
}

