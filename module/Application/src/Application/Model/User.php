<?php

namespace Application\Model;

class User {
    
    /*
     * type="integer"
     */
    protected $id = null; 
    
    /*
     * type="string",length=255
     */
    protected $username = '';
    
    /*
     * type="string",length=255
     */
    protected $email = '';
    
    /*
     * type="string",length=255
     */
    protected $password = '';
    
    /*
     * type="string",length=255
     */
    protected $passwordSalt = '';
    
    /*
     * type="string",length=255
     */
    protected $role = '';
    
    /*
     * type="string",length=255
     */
    protected $timezone = '';
    
    /*
     * saltLength - длина соли
     */
    //private $saltLength = 32;

//    public function __construct() {
//        $this->password = md5($value);
//        $this->passwordSalt = substr(md5(time()), 0, $this->saltLength);
//    }

    /**
     * Метод __set() будет выполнен при записи данных в недоступные свойства. 
     *
     * @param string $name
     * @param mixed $value
     * @return void
     * @throws UnexpectedValueException
     */
     public function __set($name, $value) {
        $method = 'set' . ucfirst($name);
        if (method_exists($this, $method)) {
            $this->$method($value);
            return;
        }
        throw new \UnexpectedValueException(sprintf(
            'The property "%s" does not exist and cannot be set',
            $name
        ));
    }   //__set
    
    
    /**
     * Метод __get() будет выполнен при чтении данных из недоступных свойств. 
     *
     * @param string $name
     * @return mixed
     * @throws UnexpectedValueException
     */
    public function __get($name) {
        // Booleans
        if ('is' == substr($name, 0, 2)) {
            if (method_exists($this, $name)) {
                return $this->$name();
            }
        }
        // Check for a getter
        $method = 'get' . ucfirst($name);
        if (method_exists($this, $method)) {
            return $this->$method();
        }
        // Unknown
        throw new \UnexpectedValueException(sprintf(
            'The property "%s" does not exist and cannot be retrieved',
            $name
        ));
    }   //__get
    
    
    /**
     * Метод __isset() будет выполнен при использовании isset() или empty() 
     * на недоступных свойствах. 
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name) {
        return property_exists($this, $name);
    }   //__isset
    
    
    /**
     * Установить Id
     * 
     * @param integer $value
     * @return Entry
     */
    public function setId($value) {
        $this->id = (int)$value;
        return $this;
    }
    
    
    /**
     * Получить Id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    
    /**
     * Установить имя юзера
     *
     * @param string $value
     * @return Entry
     */
    public function setUsername($value) {
        $this->username = substr((string)$value, 0, 255);
        return $this;
    }
    
    
    /**
     * Получить имя юзера
     *
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }
    
    
    /**
     * Установить email
     *
     * @param string $value
     * @return Entry
     */
    public function setEmail($value) {
        $this->email = substr((string)$value, 0, 255);
        return $this;
    }
    
    
    /**
     * Получить email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }
    
    
    /**
     * Установить password
     *
     * @param string $value
     * @return Entry
     */
    public function setPassword($value) {
        if (strlen($value)>255) {
            throw new \Exception('Password too long.');
        }
        $this->password = (string)$value;
        
        return $this;
    }
    
    
    /**
     * Получить password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }
    
    
    /**
     * Установить passwordSalt
     *
     * @param string $value
     * @return Entry
     */
    public function setPasswordSalt($value) {
        if (strlen($value)>255) {
            throw new \Exception('Password salt too long.');
        }
        $this->passwordSalt = (string)$value;
        
        return $this;
    }
    
    
    /**
     * Получить passwordSalt
     *
     * @return string
     */
    public function getPasswordSalt() {
        return $this->passwordSalt;
    }
    
    
    /**
     * Установить роли
     * @param string $value 
     * @return Entry
     */
    public function setRole($value) {   
        $this->role = substr((string)$value, 0, 255);
        return $this;
    }
    
    
    /**
     * Получить роли
     *
     * @return string
     */
    public function getRole() {             
        return $this->role;
    }
    
    
    /**
     * Установить временную зону 
     *
     * @param string $value time zone
     * @return Entry
     */
    public function setTimezone($value) {    
        $this->timezone = substr((string)$value, 0, 255);
        return $this;
    }
    
    
    /**
     * Получить временную зону 
     *
     * @return string
     */
    public function getTimezone() {
        return $this->timezone;
    }
    
    
    function exchangeArray($data) {
        if (isset($data['id'])) {$this->setId($data['id']);}
        if (isset($data['email'])) {$this->setEmail($data['email']);}
        if (isset($data['password'])) {$this->setPassword($data['password']);}
        if (isset($data['passwordSalt'])) {$this->setPasswordSalt($data['passwordSalt']);}
        if (isset($data['role'])) {$this->setRole($data['role']);}
        if (isset($data['username'])) {$this->setUsername($data['username']);}
        if (isset($data['timezone'])) {$this->setTimezone($data['timezone']);}
    }
    
  
    
    /*
     * Получаем соль
     */
    public function generateSalt()
    {
        $saltLength = 32;
        return substr(md5(time()), 0, $saltLength); //TODO вынести длину соли в конфиг
    }
    
    
    /*
     * Вычисляем хеш из пароля
     */
    public function getMd5Hash($value)
    {
        return  md5($value);
    }
    
}   //class User