<?php

namespace Application\View\Helper;

//use Zend\View\Helper\AbstractHelper;
use Zend\Form\View\Helper\FormTextarea;
use Zend\Form\ElementInterface;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

//use HTMLPurifier;
//use HTMLPurifier_Config;
//use HTMLPurifier_ConfigSchema;
//use Soflomo\Purifier\View\Helper\Purifier;

/**
 * Фильтрация в элементе textarea формы articleEditForm, разрешает html теги
 */
class PurefierFormTextarea extends FormTextarea implements ServiceLocatorAwareInterface {
    
    protected $services;
    
    
    public function render(ElementInterface $element)
    {
        $name   = $element->getName();
        if (empty($name) && $name !== 0) {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }
        
        $purifier = $this->getServiceLocator()->getServiceLocator()->get('HTMLPurifier');
        
        $attributes         = $element->getAttributes();
        $attributes['name'] = $name;
        $content            = (string) $element->getValue();
                
        //$fac = new \Soflomo\Purifier\Factory\HtmlPurifierFactory();
        //$fac->createService($sl);
       //$htmlPurefierHelper = new \Soflomo\Purifier\View\Helper\Purifier($fac);
        
       //$escapeHtml = $purifier->purify($content);
                
       //$escapeHtml = $purifier;
               
                    //$purifier = $this->getServiceLocator()->get('HTMLPurifier');
                    //$htmlPurefierHelper = new \View\Helper\Purifier($purifier);
                    
        //$escapeHtml         = $htmlPurefierHelper;

        return sprintf(
            '<textarea %s>%s</textarea>',
            $this->createAttributesString($attributes),
            $purifier->purify(str_replace('<br>', PHP_EOL, $content))
        );
    }  
   
    


    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }
   
    
}   //PurefierFormTextarea