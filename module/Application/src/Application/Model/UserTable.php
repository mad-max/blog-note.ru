<?php

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;



class UserTable {
    
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway=$tableGateway;
    }
    
    
    /*
     * Сохранить юзера в бд
     */
    public function saveUser(User $user) {
        
        $data=array(
            'username'=>$user->username,
            'email'=>$user->email,
            'password'=>$user->password,
            'passwordSalt'=>$user->passwordSalt,
            'role'=>$user->role,
            'timezone'=>$user->timezone,
        );
        $id=(int)$user->id;
        if ($id==null) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUser($id)) {
                $this->tableGateway->update($data, array('id'=>$id));
            } else {
                throw new \Exception('User ID does not exist');
            }
        }
    }
    
    
    /**
     * Извлечь данные юзера из таблицы
     * 
     * @return array object article
     */
    public function getUserByEmail($userEmail) {
        $rowset = $this->tableGateway->select(array('email' => $userEmail));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $userEmail");
        }
        return $row;
    }
    
    /**
     * Извлечь всех юзеров из таблицы
     * 
     * @return array objects user
     */
    public function fetchAll() {
                
        $resultSet = $this->tableGateway->select();
        return $resultSet;
        
    }   //fetchAll
    
    /**
     * Извлечь данные юзера из таблицы
     * 
     * @return array object article
     */
    public function getUser($id) {
        $id=(int)$id;
        $rowset=$this->tableGateway->select(array('id'=>$id));  //получаем простой объект ResultSet на основе данных Adapter
        $row=$rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }   //getUser
    
    
    
    
}   //class UserTable


