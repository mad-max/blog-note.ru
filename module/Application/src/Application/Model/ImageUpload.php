<?php

namespace Application\Model;

class ImageUpload {
    
    protected $id = null;   //type="integer"
    protected $filename = '';  //type="string",length=255
    protected $label = '';  //type="string",length=255
    protected $authorId = '';    //type="integer"
    protected $uploaded = '';    //type="timestamp"
     
    
    /**
     * Метод __set() будет выполнен при записи данных в недоступные свойства. 
     *
     * @param string $name
     * @param mixed $value
     * @return void
     * @throws UnexpectedValueException
     */
     public function __set($name, $value) {
        $method = 'set' . ucfirst($name);
        if (method_exists($this, $method)) {
            $this->$method($value);
            return;
        }
        throw new \UnexpectedValueException(sprintf(
            'The property "%s" does not exist and cannot be set',
            $name
        ));
    }   //__set
    
    
    /**
     * Метод __get() будет выполнен при чтении данных из недоступных свойств. 
     *
     * @param string $name
     * @return mixed
     * @throws UnexpectedValueException
     */
    public function __get($name) {
        // Booleans
        if ('is' == substr($name, 0, 2)) {
            if (method_exists($this, $name)) {
                return $this->$name();
            }
        }
        // Check for a getter
        $method = 'get' . ucfirst($name);
        if (method_exists($this, $method)) {
            return $this->$method();
        }
        // Unknown
        throw new \UnexpectedValueException(sprintf(
            'The property "%s" does not exist and cannot be retrieved',
            $name
        ));
    }   //__get
    
    
    /**
     * Метод __isset() будет выполнен при использовании isset() или empty() 
     * на недоступных свойствах. 
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name) {
        return property_exists($this, $name);
    }   //__isset
    
    
    /**
     * Установить Id
     * 
     * @param integer $value
     * @return Entry
     */
    public function setId($value) {
        $this->id = (int)$value;
        return $this;
    }
    
    
    /**
     * Получить Id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    
    /**
     * Установить имя файла
     *
     * @param string $value
     * @return Entry
     */
    public function setFilename($value) {
        $this->filename = substr((string)$value, 0, 255);
        return $this;
    }
    
    
    /**
     * Получить имя файла
     *
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }

    
    /**
     * Установить описание картинки
     *
     * @param string $value
     * @return Entry
     */
    public function setLabel($value) {
        $this->label = substr((string)$value, 0, 255);
        return $this;
    }
    
    
    /**
     * Получить текст статьи
     *
     * @return string
     */
    public function getLabel() {
        return $this->label;
    }

    
    /**
     * Установить id загрузившего картинку
     *
     * @param int $value
     * @return Entry
     */
    public function setAuthorId($value) {
        $this->authorId = (int)$value;
        return $this;
    }
    
    
    /**
     * Получить id загрузившего картинку
     *
     * @return string
     */
    public function getAuthorId() {
        return $this->authorId;
    }
    
    
    /**
     * Установить дату и время создания статьи
     *
     * @param string $value Date and Time
     * @return Entry
     */
    public function setUploaded($value = NULL) {    
        $this->uploaded = (string)$value;    
        return $this;
    }
    
    
    /**
     * Получить отформатированную дату и время создания статьи.
     *
     * @return string
     */
    public function getUploaded() {
        return $this->uploaded;
    }
    
    
    
    
    function exchangeArray($data) {
        if (isset($data['id'])) {$this->setId($data['id']);}
        if (isset($data['filename'])) {$this->setFilename($data['filename']);}
        if (isset($data['label'])) {$this->setLabel($data['label']);}
        if (isset($data['authorId'])) {$this->setAuthorId($data['authorId']);}
        if (isset($data['uploaded'])) {$this->setUploaded($data['uploaded']);}
        
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    
    
    
}   //ImageUpload

