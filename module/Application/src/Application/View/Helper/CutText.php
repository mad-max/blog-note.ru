<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Обрезает строку до нужного кол-ва символов.
 */
class CutText extends AbstractHelper {
    
    /**
     * @param string $text
     * @param int $length
     * @return string
     */
    public function __invoke($text, $length) {

        if (mb_strlen($text, 'UTF-8')>$length) {    
            //возвращает подстроку строки string длиной length, начинающегося с start символа по счету.
            $substring = mb_substr($text, 0, $length, 'UTF-8');
            
            //Возвращает позицию последнего вхождения символа
            $substringLimited = substr($substring, 0, strrpos($substring, ' ' ));
        
            //Проверяем последний символ. Если точка, то добавляем две точки, 
            //если нет, то - три.
            return (mb_substr($substringLimited, -1, 1, 'UTF-8') != '.') ? 
                    $substringLimited . '...' : $substringLimited . '..'; 
        } else {
            return $text;
        }
        
         
    }   //__invoke
    
}   //CutText