<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Application\Model\Article;
use Application\Model\ArticleTable;

use Application\Model\User;
use Application\Model\UserTable;

use Application\Model\Category;
use Application\Model\CategoryTable;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
                        
        //$eventManager->attach(MvcEvent::EVENT_RENDER, array($this, 'setFormToView'), 100);
        
        $sm  = $e->getApplication()->getServiceManager();
        
        //Форма поиска для макетов
        $eventManager->attach(MvcEvent::EVENT_RENDER, function($e) use ($sm) {
            $searchForm = $sm->get('SearchForm');
            $viewModel = $e->getViewModel();
            $viewModel->setVariables(array(
                'form' => $searchForm,
            ));
        });
        
        $auth = $sm->get('AuthService');
        
        $sharedEventManager = $eventManager->getSharedManager(); //общий менеджер событий
        
        $sharedEventManager->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, function($e) use ($auth) {
            $controller = $e->getTarget(); //обслуживаемый контроллер
//            $controllerName = $controller->getEvent()->getRouteMatch()->
//                    getParam('controller');
//            if (!in_array($controllerName, array(
//                'Users\Controller\Index', 'Users\Controller\Register',
//                'Users\Controller\Login'))) {
//                $controller->layout('layout/myaccount');
//                }
            if ($auth->hasIdentity()) {
                $controller->layout('layout/logged-layout');
            }
                
        });        
        
        //Записывает в лог неперехваченные исключения.
        $sharedEventManager->attach('Zend\Mvc\Application', 'dispatch.error',
            function($e) use ($sm) {
                if ($e->getParam('exception')){
                    $sm->get('LogService')->err($e->getParam('exception'));
                }
            }
        );
        
    }
    
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    /*
     * Функция, загружающая конфигурационные данные менеджера служб
     */
    public function getServiceConfig() {
        return array(
            'abstract_factories'=>array(),
            'aliases'=>array(),
            'factories'=>array(
                
                //база данных
                'ArticleTable' => 'Application\Service\Factory\ArticleTableFactory',
                'UserTable' => 'Application\Service\Factory\UserTableFactory',
                'CategoryTable' => 'Application\Service\Factory\CategoryTableFactory',                        
                'ImageUploadTable' => 'Application\Service\Factory\ImageUploadTableFactory',
                
                
                //формы 
                'ArticleEditForm' => 'Application\Service\Factory\ArticleEditFormFactory',
                'LoginForm' => 'Application\Service\Factory\LoginFormFactory',
                'RegisterForm' => 'Application\Service\Factory\RegisterFormFactory',
                'ImageUploadForm' => 'Application\Service\Factory\ImageUploadFormFactory',
                'SearchForm' => 'Application\Service\Factory\SearchFormFactory',
                'DeleteForm' => 'Application\Service\Factory\DeleteFormFactory',
                'CategoryEditForm' => 'Application\Service\Factory\CategoryEditFormFactory',
                
                //служба аутентификации
                'AuthService' => 'Application\Service\Factory\AuthServiceFactory',
                'CurrentUserFactory' => 'Application\Service\Factory\CurrentUserFactory',
                
                //rbac
                'Rbac' => 'Application\Service\Factory\RbacFactory',
                //'CheckAccess' => 'Application\Service\Invokables\CheckAccess',
                
                //log
                'LogService' => 'Application\Service\Factory\LogFactory',
                
            ),
            'invokables' => array(
                'GetDateTime' => 'Application\Service\Invokables\GetDateTime',
                
            ),
            'services' => array(),
            'shared' => array(),
        );
    }   //getServiceConfig
    
    public function getViewHelperConfig() {
        return array(
            'invokables' => array(
                'cutText' => 'Application\View\Helper\CutText',
                'bbcode' => 'Application\View\Helper\Bbcode',
                'FormTextarea' => 'Application\View\Helper\PurefierFormTextarea',
            ),
            'factories' => array(
                'GetLastCategories' => function ($sm) {
                    $categoryTable = $sm->getServiceLocator()->get('CategoryTable');
                    return new \Application\View\Helper\GetLastCategories($categoryTable);
                },
                'GetUser' => function ($sm) {
                    $userTable = $sm->getServiceLocator()->get('UserTable');
                    return new \Application\View\Helper\GetUser($userTable);
                }, 
                'GetCategory' => function ($sm) {
                    $categoryTable = $sm->getServiceLocator()->get('CategoryTable');
                    return new \Application\View\Helper\GetCategory($categoryTable);
                },
                'FormatDate' => function ($sm) {
                    $dateTimeService = $sm->getServiceLocator()->get('GetDateTime');
                    $currentUser = $sm->getServiceLocator()->get('CurrentUserFactory');
                    return new \Application\View\Helper\FormatDate($dateTimeService, $currentUser);
                },
                'ArticleArchive' => function ($sm) {
                    $articleTable = $sm->getServiceLocator()->get('ArticleTable');
                    return new \Application\View\Helper\ArticleArchive($articleTable);
                },
                'GetRusMonth' => function ($sm) {
                    $dateTimeService = $sm->getServiceLocator()->get('GetDateTime');
                    return new \Application\View\Helper\GetRusMonth($dateTimeService);
                },
                'ArticleTags' => function ($sm) {
                    $articleTable = $sm->getServiceLocator()->get('ArticleTable');
                    return new \Application\View\Helper\ArticleTags($articleTable);
                },
            ),
        );
    }   //getViewHelperConfig
    
    

    
  
}

