<?php

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;

class CategoryTable {
    
    protected $tableGateway;
    protected $dbAdapter;
    
    
    public function __construct(TableGateway $tableGateway, \Zend\Db\Adapter\Adapter $dbAdapter) {
        $this->tableGateway=$tableGateway;
        $this->dbAdapter=$dbAdapter;
    }
    
    /**
     * Извлечь все категории из таблицы
     * 
     * @return array objects category
     */
    public function fetchAll($paginated=false) {
        if ($paginated) {
            // create a new Select object for the table articles
            $select = new Select('category');
            $select->order('id DESC'); // сортировка 'id' DESC
            // create a new result set based on the article entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Category());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            
            $paginator = new Paginator($paginatorAdapter);
                        
            return $paginator;
        }
         
        $resultSet = $this->tableGateway->select();
        return $resultSet;
        
    }   //fetchAll
    
    
    /**
     * Извлечь категорию из таблицы
     * 
     * @return array object 
     */
    public function getCategory($id) {
        $id=(int)$id;
        $rowset=$this->tableGateway->select(array('id'=>$id));  //получаем простой объект ResultSet на основе данных Adapter
        $row=$rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }   //getCategory
    
    
    /**
     * Извлечь категории из таблицы
     * 
     * @return  
     */
    public function getLastCategories() {
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select();
        $select->from('category');
        $select->order('id DESC');
        $select->limit(10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        return $results;  
        
    }   //getLastCategories
    
    
    /**
     * Сохранить категорию в таблице
     * 
     * @return array object 
     */
    public function saveCategory(Category $category) {
        
        $data=array(
            'nameCategory' => $category->nameCategory
        );
        
        $id=(int)$category->id;
        if (empty($id)) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getCategory($id)) {
                $this->tableGateway->update($data, array('id' => $id));
                return $id;
            } else {
                throw new \Exception('Category ID does not exist');
            }
        }
        
    }   //saveCategory
    
    
    /**
     * Удалить категорию в таблице
     * 
     * @return $this
     */
    public function deleteCategory($id) {
        $this->tableGateway->delete(array('id' => $id));
        return $this; 
    }
    
    
}   //class CategoryTable



