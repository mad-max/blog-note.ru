<?php

namespace Application\Controller;


use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

//use Application\Model\Article;
use Application\Model\Category;
//use Application\Model\User;
//use Application\Model\GetDateTime;

//use Application\Model\User;
//use Application\Rbac\AssertUserIdMatches;

use \Exception;




class CategoryController extends AbstractActionController {
    
    /*
     * Сервис возвращает объект DateTime для работы с датой и временем
     */
    private $dateTimeService = NULL;
    
    /*
     * Сервис возвращает объект User c данными залогиненного пользователя
     */
    private $currentUserService = NULL;
    
    /*
     * Сервис возвращает объект ArticleTable для работы с БД
     */
    private $articleTableService = NULL;
    
    /*
     * Сервис возвращает объект CategoryTable для работы с БД
     */
    private $categoryTableService = NULL;
    
    
    /*
     * возвращает список существующих категорий
     */
    public function indexAction() {

        // grab the paginator from the ArticleTable
        $paginator = $this->getCategories(); //
        
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6);
        
        // Проверка прав
        $addPermission = false;
        $editPermission = false;
        $deletePermission = false;
        $rbac = $this->getServiceLocator()->get('Rbac');
        if ($rbac->checkAccess('add.category', 'not_assert')) {
            $addPermission = true;             
        }
        if ($rbac->checkAccess('edit.category', 'not_assert')) {
            $editPermission = true;             
        }
        if ($rbac->checkAccess('delete.category', 'not_assert')) {
            $deletePermission = true;             
        }
        
        $model = new ViewModel(array(
            'paginator' => $paginator,
            'addPermission' => $addPermission,
            'editPermission' => $editPermission,
            'deletePermission' => $deletePermission,
        ));
                
        return $model;
        
                
    }   //indexAction
    
    
    /*
     * Просмотр статей определенной категории 
     */
    public function viewAction() {
        
        $categoryId = (int) $this->params()->fromRoute('id');
        
        // grab the paginator from the ArticleTable
        $paginator = $this->getArticlesByCategory($categoryId);
          
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6);
        
        // Проверка прав
        $addPermission = false;
        $rbac = $this->getServiceLocator()->get('Rbac');
        if ($rbac->checkAccess('add.article', 'not_assert')) {
            $addPermission = true;             
        }
                
        $model = new ViewModel(array(
            'paginator' => $paginator,
            'categoryId' => $categoryId,    //для работы пагинатора, иначе на следующих страницах ничего не выдает
            'addPermission' => $addPermission,
            ));
        
        return $model;
        
    }   //viewAction
    
    
    /*
     * Обрабатывает запрос на редактирование категории
     */
    public function editAction() {
        
        $categoryId = $this->params()->fromRoute('id');  //TODO проверка
        
        $category = $this->getCategory($categoryId);
                
        $rbac = $this->getServiceLocator()->get('Rbac');
        if (!$rbac->checkAccess('edit.category', 'not_assert')) {
            return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
        }
                        
        //Установить данные в фому
        $form = $this->getServiceLocator()->get('CategoryEditForm');
        $form->bind($category);
                
        $form->get('nameCategory')->setValue($category->nameCategory);
        $form->get('action')->setValue('edit'); 
        
        return new ViewModel(array(
            'category' => $category,
            'form' => $form,
        ));
        
        
    }   //EditAction
    
    
    /*
     * Выводит форму для добавления категории
     */
    public function addAction() {
        
        //Проверка разрешения
        $rbac = $this->getServiceLocator()->get('Rbac');      
        if (!$rbac->checkAccess('add.category', 'not_assert')) {
            return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
        }
        
        $form = $this->getServiceLocator()->get('CategoryEditForm');
        $form->get('action')->setValue('add'); 
        
        return new ViewModel(array(
            'form' => $form,
        ));
        
    }   //AddAction
    
    
   
    
    /*
     * Сохранение категории в бд
     */
    public function processAction() {
             
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $this->request->getPost();
            $form = $this->getServiceLocator()->get('CategoryEditForm'); 
        
            $form->setData($post);  
            if (!$form->isValid()) {    
                //return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой
                if (!$form->get('id')->getValue()) {     //Редактирование 
                    return $this->getErrorModel($form, 'application/category/add');
                }
                return $this->getErrorModel($form, 'application/category/edit');  
            }
            
            $validatedData = $form->getData();
            
            if (!empty($validatedData['id'])) {
                //Проверка разрешения
                $rbac = $this->getServiceLocator()->get('Rbac');
                if (!$rbac->checkAccess('edit.category', 'not_assert')) {
                    return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
                }
                $category = $this->getCategory($validatedData['id']);   //Получить категорию из БД
            } else {
                //Проверка разрешения
                $rbac = $this->getServiceLocator()->get('Rbac');
                if (!$rbac->checkAccess('add.category', 'not_assert')) {
                    return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
                }
                $category = new Category(); 
                
            }
            
            $category->exchangeArray($validatedData);
            
            //сохраняем категрию
            $this->saveCategory($category);
            
        }
        
        return $this->redirect()->toRoute('application/category');
            
            
    }   //processAction
    
    
    /*
     * Удалить категорию из бд
     */
    public function deleteAction() {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $this->request->getPost();
            $form = $this->getServiceLocator()->get('DeleteForm'); 
        
            $form->setData($post);  
            if (!$form->isValid()) {    
                return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой
            }
            
            $validatedData = $form->getData();
            
            if ($validatedData['action'] === 'delete') {
                
                //Получить категорию из БД
                $category = $this->getCategory($validatedData['id']);

                //Проверка разрешения
                $rbac = $this->getServiceLocator()->get('Rbac');
                if (!$rbac->checkAccess('delete.category', 'not_assert')) {
                    return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
                }
                
                //Удаляем категорию
                $this->deleteCategory($category->id);
            }
            
            //Переход на страницу со списком статей
            return $this->redirect()->toRoute('application/category');
            
        }
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('application/category');
        }
        
        //Получить статью из БД
        $category = $this->getCategory($id);
        
        //Возвратить форму подтверждения удаления
        $form = $this->getServiceLocator()->get('DeleteForm');
        $form->get('action')->setValue('delete'); 
        $form->get('id')->setValue($category->id); 
         
        return new ViewModel(array(
            'form' => $form,
            'category' => $category,
        ));
        
        
    }   //deleteAction

    
    /*
     * Получить список категорий
     * 
     * возвращает массив id категории => название категории
     */
    private function getCategories() {

       //Получить список объектов категория
        try {
            $categoryTable = $this->getServiceLocator()->get('CategoryTable');
            $categories = $categoryTable->fetchAll(true);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
        
        //Установить значения в список с категориями
//        $categoryList = array();
//        foreach($categories as $categoryObject) {
//            $categoryList[$categoryObject->id] = $categoryObject->nameCategory;
//        }
        
        return $categories;
    }   //getCategories
            
    
    /*
     * Получить категорию
     * 
     */
    private function getCategory($id) {

       //Получить список объектов категория
        try {
            $categoryTable = $this->getServiceLocator()->get('CategoryTable');
            $category = $categoryTable->getCategory($id);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
        
        
        return $category;
    }   //getCategory

     /*
     * Получить категорию
     * 
     */
    private function saveCategory($category) {

        try {
            return $this->getCategoryTableService()->saveCategory($category);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }   //saveCategory
    
    
    /*
     * Удаляет Category из БД по id
     */
    private function deleteCategory($id) {
        try {
            $this->getCategoryTableService()->deleteCategory($id);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
        return $this;
    }
    
    
    /*
     * Возвращает объект ArticleTable для работы с БД
     */
    private function getArticleTableService() {
        if($this->articleTableService===NULL) {
            $this->articleTableService = $this->getServiceLocator()->get('ArticleTable');
        }
        return $this->articleTableService;
    }
    
    /*
     * Возвращает объект CategoryTable для работы с БД
     */
    private function getCategoryTableService() {
        if($this->categoryTableService===NULL) {
            $this->categoryTableService = $this->getServiceLocator()->get('CategoryTable');
        }
        return $this->categoryTableService;
    }
    
    /*
     * Получить статьи по категории
     */
    private function getArticlesByCategory($id) {
        //Получить статьи
        try {
            return $this->getArticleTableService()->getArticleByCategory($id, true);    //true для работы пагинатора
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
     
    /**
     * Создает модель с описанием ошибки (если требется) в случае если не 
     * прошла валидацию или исключение при сохранении в бд, или пароли не 
     * совпадают и т.д.
     */
    private function getErrorModel($form, $template, $errorDescription = null) {
        
        $dataModel = array(
            'error'=>true,
            'form'=>$form,
        );
        
        if (isset($errorDescription)) {
            $dataModel['description'] = $errorDescription;
        }
        
        $model=new ViewModel($dataModel);
        $model->setTemplate($template);
        
        return $model;
        
    }
    
    
}   //CategoryController

