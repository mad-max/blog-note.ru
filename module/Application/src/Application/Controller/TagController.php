<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;




use \Exception;


class TagController extends AbstractActionController {
    
    /*
     * Сервис возвращает объект ArticleTable для работы с БД
     */
    private $articleTableService = NULL;
    
    
    /*
     * возвращает список тегов
     */
    public function indexAction() {
        
        // grab the paginator from the ArticleTable
        $paginator = $this->getTags(); //
        
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6);
               
        $model = new ViewModel(array(
            'paginator' => $paginator,
        ));
                
        return $model;
        
    }   //indexAction
    
    
    /*
     *  возвращает список статей с определенным тегом
     */
    public function viewAction() {
        
        $unverifiedTag = $this->params()->fromRoute('tag');
        
        //Проверка тега
        $filter = new \Zend\I18n\Filter\Alnum();
        $tag = $filter->filter($unverifiedTag);   
        
        // grab the paginator from the ArticleTable
        $paginator = $this->getArticlesByTag($tag);
        
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6);
        
        // Проверка прав
        $addPermission = false;
        $rbac = $this->getServiceLocator()->get('Rbac');
        if ($rbac->checkAccess('add.article', 'not_assert')) {
            $addPermission = true;             
        }
        
        $model = new ViewModel(array(
            'paginator' => $paginator,
            'addPermission' => $addPermission,
            'tag' => $tag,
        ));
          
        return $model;
        
    }   //viewAction
    
        
    /*
     * Получить статьи с тегом
     */
    private function getArticlesByTag($tag) {
        //Получить статьи
        try {
            return $this->getArticleTableService()->getArticlesByTag($tag, true);    //true для работы пагинатора
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
    
    /*
     * Возвращает объект ArticleTable для работы с БД
     */
    private function getArticleTableService() {
        if($this->articleTableService===NULL) {
            $this->articleTableService = $this->getServiceLocator()->get('ArticleTable');
        }
        return $this->articleTableService;
    }
    
    
    /*
     * Получить список тегов
     * 
     * возвращает объект пагинатор
     */
    private function getTags() {

       //Получить список тегов
        try {
            $articleTable = $this->getServiceLocator()->get('ArticleTable');
            $tags = $articleTable->getTags(true);
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
                
        return $tags;
        
    }   //getTags

    
    
}   //TagController
