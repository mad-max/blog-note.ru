<?php

namespace Application\Form;

use Zend\Form\Form;


class DeleteForm extends Form {
    
    public function __construct($name=null) {
        parent::__construct('Delete');
        //$this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
       
        
        $this->add(array(
            'name'=>'action',
            'attributes'=>array(
                'type'=>'hidden',
                'value' => '',
            ),
        ));
               
        $this->add(array(
            'name'=>'id',
            'attributes'=>array(
                'type'=>'hidden',
                'value' => '',
            ),
        ));
                
        $this->add(array(
            'name'=>'confirm',
            'attributes'=>array(
                'type'=>'submit',
                'value' => 'Confirm',
                'class' => 'btn btn-default',
            ),
        ));
                        
        
        
    }   //__construct


}













