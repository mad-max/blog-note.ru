<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Извлекает все категории из бд для отображения их в представлении
 */
class GetLastCategories extends AbstractHelper {
    
    protected $categoryTable;
    
    public function __construct($categoryTable) {
        $this->categoryTable = $categoryTable;
    }

    /**
     * @return 
     */
    public function __invoke() {
        
        return $this->categoryTable->getLastCategories();
        
    }   //__invoke
    
}   //GetCategories