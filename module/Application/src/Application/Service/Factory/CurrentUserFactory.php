<?php

namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 

use Application\Model\User;

use \Exception;

/**
 * Возвращает объект User с данными залогиненного пользователя
 * Если вход не выполнен, то присваивается роль и имя anonymous.
 * 
 * @return object User 
 */
class CurrentUserFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        
        $authService = $sm->get('AuthService');
        if ($authService->hasIdentity()) {
            $storedUserData = $authService->getStorage()->read();
            try {
                $userTable = $sm->get('UserTable'); 
                $user = $userTable->getUserByEmail($storedUserData);  
            } catch (Exception $ex) {
                $user = $this->createDefaultUser();
            }
        } else {
            $user = $this->createDefaultUser();
        }
        
        return $user;
        
    }
    
    
    private function createDefaultUser() {
        $user = new User();
        $user->username = 'anonymous';
        $user->role = 'anonymous';
        $user->timezone = 'Asia/Irkutsk';  //Присвоить часовой пояс по умолчанию
        
        return $user;
        
    }
    
    
}

