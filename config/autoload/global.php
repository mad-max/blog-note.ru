<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'db'=>array(
        'driver'=>'Pdo',
        'dsn'=>'mysql:dbname=blog-note;host=127.0.0.1;',
        'username'=>'root',
        'password'=>'',
        'driver_options'=>array(
            PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES\'UTF8\''
        ),
    ),
    'service_manager'=>array(
        'factories'=>array(
            'Zend\Db\Adapter\Adapter'=>'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
    'soflomo_purifier' => array(
        'config' => array(
            'HTML.Doctype' => 'HTML 4.01 Transitional',
            'HTML.Allowed' => 'pre,br,b,i,ol,ul,li,img[src|height|width|alt|class],blockquote,code,a[href]',
            //'CSS.MaxImgLength' => '20px',
            'HTML.MaxImgLength' => '500',
            //'HTML.Trusted' => 'true',
            //'HTML.SafeEmbed' => 'true',
            //'HTML.AllowedAttributes' => 'height,width,src',
            'AutoFormat.RemoveEmpty.RemoveNbsp' => 'true',
            'AutoFormat.RemoveEmpty' => 'true',     // удаляет пустые теги
            'Core.EscapeInvalidTags' => 'true',
            
            
            //'AutoFormat.AutoParagraph' => 'true',   // авто добавление <p> в тексте при переносе
            //'Output.TidyFormat' => true,
        ),
    ),
    'static_salt'=>array(
        'hash'=>'4ab179d42f96533ab4075364910e3437',
    ),
    
    'rbac_config' => array(
        'roles' => array(
            'admin' => array(
                'permissions' => array(
                    'admin.access'
                ),
            ),
            'moderator' => array(
                'parent' => 'admin',
                'permissions' => array(
                'edit.category',
                'delete.category',    
                ),
            ),
            'author' => array(
                'parent' => 'moderator',
                'permissions' => array(
                    'add.article',
                    'edit.article',
                    'delete.article',
                    'upload.image',
                    'delete.image',
                    'add.category',
                    
                ),
            ),
            'member' => array(
                'parent' => 'author',
                'permissions' => array(
                    'add.comment',
                    'edit.comment',
                    'delete.comment'
                ),
            ),
            'anonymous' => array(
                'parent' => 'member',
            ),
        ),
    ),
    
    
);
