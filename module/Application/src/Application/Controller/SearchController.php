<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use ZendSearch\Lucene;
use ZendSearch\Lucene\Document;
use ZendSearch\Lucene\Index;

use ZendSearch\Lucene\Search\Highlighter\DefaultHighlighter;

use ZendSearch\Lucene\Document\Html;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;

use Zend\Http\Client;

class SearchController extends AbstractActionController {
    
    /*
     * Действие по умолчанию возвращает результат поиска
     */
    public function indexAction() {
        
        $request = $this->getRequest();
        //$searchResults=null;    //без этого вылазит ошибка Undefined variable
        if($request->isPost()) {
            
            $post = $request->getPost();   
        
            $form = $this->getServiceLocator()->get('SearchForm'); 
            
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array(
                    'error' => true,
                ));
                $model->setTemplate('application/search/error');
                return $model;
            }
            
            $validatedQuery = $form->getData()['searchQuery'];
            
        } else {
            $query = $this->params()->fromQuery('query', 0);
            if (!$query) {
                return $this->redirect()->toRoute('application/article');
            }
            $filter = new \Zend\I18n\Filter\Alnum(true);
            $filterTags = new \Zend\Filter\StripTags();  //TODO сделать цепочку фильтров
            $validatedQuery = $filter->filter($filterTags->filter($query));
        }
        
        
        //setlocale(LC_ALL, 'ru_RU.UTF-8');
        setlocale(LC_CTYPE, 'ru_RU.UTF-8');

        $articlesIndexLocation = $this->getArticlesIndexLocation();
        $imgIndexLocation = $this->getImgIndexLocation();

        Lucene\Analysis\Analyzer\Analyzer::setDefault(
            new Lucene\Analysis\Analyzer\Common\Utf8\CaseInsensitive()  //Для работы с русским языком (иначе выдает ошибку illegal character)
        );


        Lucene\Search\QueryParser::setDefaultEncoding('utf-8');


        $articlesIndex = Lucene\Lucene::open($articlesIndexLocation);
        

        $articlesResults = $articlesIndex->find($validatedQuery);
        
        $imgIndex = Lucene\Lucene::open($imgIndexLocation);
        //$validatedQuery = $form->getData();

        $imgResults = $imgIndex->find($validatedQuery);

        
            
        $paginatorAdapter = new ArrayAdapter($articlesResults);

        $paginator = new Paginator($paginatorAdapter);
            
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6); //TODO убрать в константу
        

        //$query = urlencode($validatedQuery);
        
        return new ViewModel(array(
            'paginator' => $paginator,
            'articlesResults' => $articlesResults,
            'imgResults' => $imgResults,
            'query' => urlencode($validatedQuery),
        ));
        
    }   //indexAction
    
    
    public function searchArticlesAction() {
        
        $query = $this->params()->fromQuery('query', 0);
        if (!$query) {
            return $this->redirect()->toRoute('application/article');
        }
        $filter = new \Zend\I18n\Filter\Alnum(true);
        $filterTags = new \Zend\Filter\StripTags();  //TODO сделать цепочку фильтров
        $validatedQuery = $filter->filter($filterTags->filter($query));

        //setlocale(LC_ALL, 'ru_RU.UTF-8');
        setlocale(LC_CTYPE, 'ru_RU.UTF-8');

        Lucene\Analysis\Analyzer\Analyzer::setDefault(
            new Lucene\Analysis\Analyzer\Common\Utf8\CaseInsensitive()  //Для работы с русским языком (иначе выдает ошибку illegal character)
        );

        Lucene\Search\QueryParser::setDefaultEncoding('utf-8');
        
        $articlesIndexLocation = $this->getArticlesIndexLocation();
        $articlesIndex = Lucene\Lucene::open($articlesIndexLocation);
        $articlesResults = $articlesIndex->find($validatedQuery);
            
        $paginatorAdapter = new ArrayAdapter($articlesResults);
        $paginator = new Paginator($paginatorAdapter);
            
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6); //TODO убрать в константу
        
        return new ViewModel(array(
            'paginator' => $paginator,
            'articlesResults' => $articlesResults,
            'query' => urlencode($validatedQuery),
        ));
        
    }   //searchArticlesAction

    
    public function searchImagesAction() {
        
        $query = $this->params()->fromQuery('query', 0);
        if (!$query) {
            return $this->redirect()->toRoute('application/media');
        }
        $filter = new \Zend\I18n\Filter\Alnum(true);
        $filterTags = new \Zend\Filter\StripTags();  //TODO сделать цепочку фильтров
        $validatedQuery = $filter->filter($filterTags->filter($query));

        //setlocale(LC_ALL, 'ru_RU.UTF-8');
        setlocale(LC_CTYPE, 'ru_RU.UTF-8');

        Lucene\Analysis\Analyzer\Analyzer::setDefault(
            new Lucene\Analysis\Analyzer\Common\Utf8\CaseInsensitive()  //Для работы с русским языком (иначе выдает ошибку illegal character)
        );

        Lucene\Search\QueryParser::setDefaultEncoding('utf-8');
        
        $imgIndexLocation = $this->getImgIndexLocation();
        $imgIndex = Lucene\Lucene::open($imgIndexLocation);
        $imgResults = $imgIndex->find($validatedQuery);
            
        $paginatorAdapter = new ArrayAdapter($imgResults);
        $paginator = new Paginator($paginatorAdapter);
            
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6); //TODO убрать в константу
        
        return new ViewModel(array(
            'paginator' => $paginator,
            'imgResults' => $imgResults,
            'query' => urlencode($validatedQuery),
        ));
        
    }   //searchArticlesAction
    

    public function generateIndexAction() {
        
        // Проверка прав
        $rbac = $this->getServiceLocator()->get('Rbac');
        if (!$rbac->checkAccess('admin.access')) {
            return $this->notFoundAction();             //TODO перенаправление на страницу с ошибкой доступ запрещен
        }
        
        //TODO Поиск по остальным таблицам и файлам
        
        //setlocale(LC_ALL, 'ru_RU.UTF-8');
        setlocale(LC_CTYPE, 'ru_RU.UTF-8');
        /*
         * Для работы с русским языком (иначе выдает ошибку illegal character)
         */
        Lucene\Analysis\Analyzer\Analyzer::setDefault(
            new Lucene\Analysis\Analyzer\Common\Utf8\CaseInsensitive()
        );
      
        Lucene\Search\QueryParser::setDefaultEncoding('utf-8');
        
        $articlesIndexLocation = $this->getArticlesIndexLocation();
        $index = Lucene\Lucene::create($articlesIndexLocation);
                
        $articleTable = $this->getServiceLocator()->get('ArticleTable');
        $allArticles = $articleTable->fetchAll();
        
        foreach($allArticles as $article) {
            //создание полей lucene
            $articleId = Document\Field::unIndexed('articleId', $article->id);    
            $summary = Document\Field::Text('summary', $article->summary); 
            $content = Document\Field::Text('content', $article->content); 
            $title = Document\Field::Text('title', $article->title); 
            
            //создание нового документа
            $indexDoc = new Lucene\Document();
                            
            //добавление всех полей
            $indexDoc->addField($title);
            $indexDoc->addField($summary);
            $indexDoc->addField($content);
            $indexDoc->addField($articleId);
            $index->addDocument($indexDoc);
        }
        $index->optimize();
        $index->commit();
        
        $imgIndexLocation = $this->getImgIndexLocation();
        $indexImage = Lucene\Lucene::create($imgIndexLocation);
        
        $imageUploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $allImages = $imageUploadTable->fetchAll();
        
        foreach($allImages as $image) {
            //создание полей lucene
            $imageId = Document\Field::unIndexed('imageId', $image->id);    
            $imageLabel = Document\Field::Text('label', $image->label); 
            
            //создание нового документа
            $indexDoc = new Lucene\Document();
                            
            //добавление всех полей
            $indexDoc->addField($imageId);
            $indexDoc->addField($imageLabel);
            $indexImage->addDocument($indexDoc);
        }
        $index->optimize();
        $index->commit();
        
    }   //generateIndexAction
     
    
    
    
    
    
    /*
     * Получение конфигурации из конфигурационных данных модуля
     */
    private function getArticlesIndexLocation() {
        $config  = $this->getServiceLocator()->get('config');
        if ($config instanceof \Traversable) { 
            $config = ArrayUtils::iteratorToArray($config);
        }
        if (!empty($config['module_config']['article_index_location'])) {
            return $config['module_config']['article_index_location'];
        } else {
            throw new \Exception("Could not find articles index location");
        }
    }
    
    /*
     * Получение конфигурации из конфигурационных данных модуля
     */
    private function getImgIndexLocation() {
        $config  = $this->getServiceLocator()->get('config');
        if ($config instanceof \Traversable) { 
            $config = ArrayUtils::iteratorToArray($config);
        }
        if (!empty($config['module_config']['img_index_location'])) {
            return $config['module_config']['img_index_location'];
        } else {
            throw new \Exception("Could not find articles index location");
        }
    }
    
    
    
    
}   //SearchController
