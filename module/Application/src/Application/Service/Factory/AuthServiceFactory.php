<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

/**
 * Создает службу аутентификации
 * 
 * @return $authService
 */
class AuthServiceFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $config = $sm->get('config');
        $static_salt = $config['static_salt']['hash'];
        $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'users', 
            'email', 'password', "MD5(CONCAT('".$static_salt."', ?, passwordSalt))");  
        $authService = new AuthenticationService();
        $authService->setAdapter($dbTableAuthAdapter);
        return $authService;
    }
}


