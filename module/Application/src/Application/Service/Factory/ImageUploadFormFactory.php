<?php

namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 

use Application\Filter\ImageUploadFilter;
use Application\Form\ImageUploadForm;


/**
 * Создает форму редактирования статьи
 * 
 * @return ImageUploadForm
 */
//class ImageUploadFormFactory {
//    public function __construct($name = null, $options = array()) {
//        $filter = new ImageEditFilter();
//        $form = new ImageUploadForm($name = null, $options = array());
//        $form->setInputFilter($filter);
//        return $form;
//    }
//}

class ImageUploadFormFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $filter = new ImageUploadFilter();
        $form = new ImageUploadForm();
        $form->setInputFilter($filter);
        return $form;
    }
}