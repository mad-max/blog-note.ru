<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Application\Model\Article;
use Application\Model\ArticleTable;

/**
 * Создает объект для работы с таблицей articles
 * 
 * @return ArticleTable
 */
class ArticleTableFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Article());
        $tableGateway = new TableGateway('articles', $dbAdapter, null, $resultSetPrototype);
        $table = new ArticleTable($tableGateway, $dbAdapter);
        return $table;
    }
}
