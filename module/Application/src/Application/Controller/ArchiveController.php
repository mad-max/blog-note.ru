<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Filter\Digits;


use \Exception;


class ArchiveController extends AbstractActionController {
    
    /*
     * Сервис возвращает объект ArticleTable для работы с БД
     */
    private $articleTableService = NULL;
    
    /*
     * Сервис возвращает объект DateTime для работы с датой и временем
     */
    //private $dateTimeService = NULL;
    
    /*
     * возвращает список статей архив за определенный месяц или год
     * TODO поменять с view
     */
    public function indexAction() {
         
        //TODO list of archives
        $results = $this->getArrayOfArchiveDates();
        $model = new ViewModel(array(
            'dates' => $results,
        ));
          
        return $model;
        
    }   //indexAction
    
    
    /*
     * возвращает архив (список всех годов и месяцев со статьями)
     */
    public function viewAction() {
        
        $year = $this->params()->fromRoute('year');
        $month = $this->params()->fromRoute('month');
                
        $filter = new Digits();
        $validatedYear = $filter->filter($year);
        $validatedMonth = $filter->filter($month);
        if ($validatedMonth <1 or $validatedMonth >12) {
            $validatedMonth = NULL;
        }
                
        // grab the paginator from the ArticleTable
        $paginator = $this->getArticleArchive($validatedYear, $validatedMonth);
        
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(6);
        
        // Проверка прав
        $addPermission = false;
        $rbac = $this->getServiceLocator()->get('Rbac');
        if ($rbac->checkAccess('add.article', 'not_assert')) {
            $addPermission = true;             
        }
        
        $model = new ViewModel(array(
            'paginator' => $paginator,
            'addPermission' => $addPermission,
            'selectedMonth' => $validatedMonth,
            'selectedYear' => $validatedYear,
        ));
          
        return $model;
        
    }   //viewAction
    
    
    /*
     * Получить архив статей за год и/или месяц
     */
    private function getArticleArchive($year, $month) {
        //Получить статьи
        try {
            return $this->getArticleTableService()->getArticleArchive($year, $month, true);    //true для работы пагинатора
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }

    /*
     * Получить архив статей
     */
    private function getArrayOfArchiveDates() {
        //Получить статьи
        try {
            return $this->getArticleTableService()->getArrayOfArchiveDates();
        } catch (Exception $ex) {
            $this->getServiceLocator()->get('LogService')->err($ex);
            return $this->notFoundAction();
        }
    }
    
    /*
     * Возвращает объект ArticleTable для работы с БД
     */
    private function getArticleTableService() {
        if($this->articleTableService===NULL) {
            $this->articleTableService = $this->getServiceLocator()->get('ArticleTable');
        }
        return $this->articleTableService;
    }
    

    
    
}   //ArchiveController
