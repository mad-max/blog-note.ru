<?php

namespace Application\Form;

use Zend\Form\Form;


class RegisterForm extends Form {
    
    public function __construct($name=null) {
        parent::__construct('Register');
        //$this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
       
        
        $this->add(array(
            'name'=>'username',
            'attributes'=>array(
                'type'=>'text',
                'required' => 'required',
                'autofocus' => "true",
            ),
            'options'=>array(
                'label'=>'Name',
            ),
        ));
        
        
        $this->add(array(
            'name'=>'email',
            'attributes'=>array(
                'id' => 'email',
                'type'=>'email',
                'required' => 'required',
            ),
            'options'=>array(
                'label' => 'Email address',
            ),
        ));
        
        
        $this->add(array(
            'name'=>'timezone',
            'type' => 'Zend\Form\Element\Select',
            'attributes'=>array(
                'type'=>'select',
                'required' => 'required',
            ),
            'options'=>array(
                'label'=>'Timezone',
                'disable_inarray_validator' => true,    //отключить валидатор InArray. 
            ),
        ));
        
                
        $this->add(array(
            'name'=>'password',
            'attributes'=>array(
                'id' => 'password',
                'type'=>'password',
                'required' => 'required',
            ),
            'options'=>array(
                'label' => 'Password',
                
                
            ),
        ));
        
        $this->add(array(
            'name'=>'confirmPassword',
            'attributes'=>array(
                'id' => 'confirmPassword',
                'type'=>'password',
                'required' => 'required',
            ),
            'options'=>array(
                'label' => 'Confirm password',
                
                
            ),
        ));
        
        
                
        $this->add(array(
            'name'=>'register',
            'attributes'=>array(
                'type'=>'submit',
                'value' => 'Register',
            ),
        ));
                        
        
        
    }   //__construct


}














