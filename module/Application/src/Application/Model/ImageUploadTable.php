<?php

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
 

class ImageUploadTable {
    
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway=$tableGateway;
    }
    
    

    /**
     * Извлечь все картинки из таблицы
     * 
     * @return paginator or array objects article
     */
    public function fetchAll($paginated=false) {
        
        if ($paginated) {
            // create a new Select object for the table articles
            $select = new Select('image_uploads');
            $select->order('id DESC'); // сортировка 'id' DESC
            // create a new result set based on the article entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ImageUpload());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            
            $paginator = new Paginator($paginatorAdapter);
                        
            return $paginator;
        }
         
        $resultSet = $this->tableGateway->select();
        return $resultSet;
        
    }   //fetchAll
    
    /**
     * Извлечь картинку из таблицы
     * 
     * @return array object article
     */
    public function getImageUpload($id) {
        $id=(int)$id;
        $rowset=$this->tableGateway->select(array('id'=>$id));  //получаем простой объект ResultSet на основе данных Adapter
        $row=$rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }   //getImageUpload
    
    
    /**
     * Сохранить картинку в таблице
     * 
     * @return array object article
     */
    public function saveImageUpload(ImageUpload $imageUpload) {
        
        $data=array(
            'filename'=>$imageUpload->filename,
            'label'=>$imageUpload->label,
            'authorId'=>$imageUpload->authorId,
            'uploaded'=>$imageUpload->uploaded,
        );
        $id=(int)$imageUpload->id;
        if (empty($id)) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getImageUpload($id)) {
                $this->tableGateway->update($data, array('id'=>$id));
                return $id;
            } else {
                throw new \Exception('Article ID does not exist');
            }
        }
        
    }   //saveImageUpload
    
    
    public function deleteImageUpload($id) {
        $this->tableGateway->delete(array('id' => $id));
    }
    
}   //ImageUploadTable

