<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

use \Exception;

/**
 * Извлекает все категории из бд для отображения их в представлении
 */
class GetCategory extends AbstractHelper {
    
    //protected $categoryTable;
    
    public function __construct($categoryTable) {
        $this->categoryTable = $categoryTable;
    }

    /**
     * @return 
     */
    public function __invoke($categoryId=null) {
        $id = (int)$categoryId;
        try {
            $category = $this->categoryTable->getCategory($id);
            return $category->nameCategory;
        } catch (Exception $ex) {
            return $category=null;
        }
    }   //__invoke
    
}   //GetCategory
