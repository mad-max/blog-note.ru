<?php
namespace Application\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface; 

use Application\Filter\RegisterFilter;
use Application\Form\RegisterForm;

/**
 * Создает форму для входа
 * 
 * @return RegisterForm
 */
class RegisterFormFactory implements FactoryInterface {
    public function createService(ServiceLocatorInterface $sm) {
        $filter = new RegisterFilter();
        $form = new RegisterForm();
        $form->setInputFilter($filter);
        return $form;
    }
}
