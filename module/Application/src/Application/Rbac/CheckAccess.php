<?php

namespace Application\Rbac;

use Application\Rbac\AssertUserIdMatches;
//use Application\Model\Article;
use Application\Model\User;
use Zend\Permissions\Rbac\Rbac;

/**
 * Создает объект для проверки прав пользователя
 * 
 * @return CheckAccess
 */
class CheckAccess {
    /*
     * Текущий пользователь
     */
    private $currentUser=null;
            
    /*
     * Объект (настроенный) Rbac
     */        
    private $rbac=null;
    
    /*
     * Для проверки совпадений id статьи и автора
     */
    private $assertion=null;
    
    
    public function __construct(User $currentUser,Rbac $rbac) {
        $this->currentUser = $currentUser;
        $this->rbac = $rbac;
        $this->assertion = new AssertUserIdMatches($this->currentUser);
        
    }
    
    public function checkAccess($action, $article=null) {
        $action = (string)$action;
        if ($article==='not_assert' or $this->currentUser->role==='moderator' or $this->currentUser->role==='admin') {
            return $this->rbac->isGranted($this->currentUser->role, $action);
        } else {
            $this->assertion->setArticle($article);
            return $this->rbac->isGranted($this->currentUser->role, $action, $this->assertion);
        }
    }
    
}

